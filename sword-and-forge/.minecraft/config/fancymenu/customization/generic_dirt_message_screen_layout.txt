type = fancymenu_layout

layout-meta {
  identifier = generic_dirt_message_screen
  render_custom_elements_behind_vanilla = false
  last_edited_time = 1716105062717
  is_enabled = true
  randommode = false
  randomgroup = 1
  randomonlyfirsttime = false
  layout_index = 0
  [loading_requirement_container_meta:4064bd62-03e4-404f-a00f-b83bf397a6f4-1716105030213] = [groups:][instances:]
}

customization {
  action = backgroundoptions
  keepaspectratio = false
}

scroll_list_customization {
  preserve_scroll_list_header_footer_aspect_ratio = true
  render_scroll_list_header_shadow = true
  render_scroll_list_footer_shadow = true
  show_scroll_list_header_footer_preview_in_editor = false
  repeat_scroll_list_header_texture = false
  repeat_scroll_list_footer_texture = false
}

element {
  copy_client_player = true
  playername = _Cuddles
  auto_skin = false
  auto_cape = false
  slim = false
  skin_source = _Cuddles
  scale = 30
  parrot = false
  parrot_left_shoulder = false
  is_baby = false
  crouching = false
  showname = true
  head_follows_mouse = true
  body_follows_mouse = true
  body_x_rot_advanced_mode = false
  body_y_rot_advanced_mode = false
  head_x_rot_advanced_mode = false
  head_y_rot_advanced_mode = false
  head_z_rot_advanced_mode = false
  left_arm_x_rot_advanced_mode = false
  left_arm_y_rot_advanced_mode = false
  left_arm_z_rot_advanced_mode = false
  right_arm_x_rot_advanced_mode = false
  right_arm_y_rot_advanced_mode = false
  right_arm_z_rot_advanced_mode = false
  left_leg_x_rot_advanced_mode = false
  left_leg_y_rot_advanced_mode = false
  left_leg_z_rot_advanced_mode = false
  right_leg_x_rot_advanced_mode = false
  right_leg_y_rot_advanced_mode = false
  right_leg_z_rot_advanced_mode = false
  element_type = fancymenu_customization_player_entity
  instance_identifier = e37250a9-a0bc-4a2b-b577-417049dc29ef-1716105044310
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = mid-centered
  x = -9
  y = -27
  width = 18
  height = 54
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 6056ab1a-2228-4c06-9fa2-718c6873c760-1716105044310
  [loading_requirement_container_meta:6056ab1a-2228-4c06-9fa2-718c6873c760-1716105044310] = [groups:][instances:]
}

vanilla_button {
  button_element_executable_block_identifier = efef0f52-f9c6-4f30-984c-134125afd809-1716105030213
  [executable_block:efef0f52-f9c6-4f30-984c-134125afd809-1716105030213][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = message
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 327
  y = 70
  width = 200
  height = 9
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = a3105408-02ed-46c2-be4f-bfca38eb00d5-1716105030213
  [loading_requirement_container_meta:a3105408-02ed-46c2-be4f-bfca38eb00d5-1716105030213] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

