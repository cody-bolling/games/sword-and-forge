type = fancymenu_layout

layout-meta {
  identifier = pause_screen
  render_custom_elements_behind_vanilla = false
  last_edited_time = 1716146695524
  is_enabled = true
  randommode = false
  randomgroup = 1
  randomonlyfirsttime = false
  layout_index = 0
  [loading_requirement_container_meta:a2605860-0074-4e19-aace-fc1fe451b0cf-1716146482577] = [groups:][instances:]
}

customization {
  action = backgroundoptions
  keepaspectratio = false
}

scroll_list_customization {
  preserve_scroll_list_header_footer_aspect_ratio = true
  render_scroll_list_header_shadow = true
  render_scroll_list_footer_shadow = true
  show_scroll_list_header_footer_preview_in_editor = false
  repeat_scroll_list_header_texture = false
  repeat_scroll_list_footer_texture = false
}

vanilla_button {
  button_element_executable_block_identifier = d186c8fd-660f-4072-807d-f3551efa8ea6-1716146482578
  [executable_block:d186c8fd-660f-4072-807d-f3551efa8ea6-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 374306
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 301
  y = 176
  width = 20
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 86b8e84f-37b7-43fe-8f3d-0b579ff6ae8c-1716146482578
  [loading_requirement_container_meta:86b8e84f-37b7-43fe-8f3d-0b579ff6ae8c-1716146482578] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 7e6d37ea-6848-4ee8-ab08-6aa612840404-1716146482578
  [executable_block:7e6d37ea-6848-4ee8-ab08-6aa612840404-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 374282
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 301
  y = 152
  width = 20
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 471434d2-fb76-46a4-95b0-57816bb5dadd-1716146482578
  [loading_requirement_container_meta:471434d2-fb76-46a4-95b0-57816bb5dadd-1716146482578] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 44edc844-80ba-447e-8188-9aa9e27f6f72-1716146482578
  [executable_block:44edc844-80ba-447e-8188-9aa9e27f6f72-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_return_to_game_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = mc_pausescreen_advancements_button
  x = 0
  y = -27
  width = 204
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = cf6a62d1-b123-482e-8e08-ec3e5aa57925-1716146482578
  [loading_requirement_container_meta:cf6a62d1-b123-482e-8e08-ec3e5aa57925-1716146482578] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = d7afe5e3-030a-4c87-9568-5e978a5f7e76-1716146482578
  [executable_block:d7afe5e3-030a-4c87-9568-5e978a5f7e76-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 398354
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 325
  y = 224
  width = 204
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = a4a2ea5e-4a2f-449c-a20b-78144af692d9-1716146482578
  [loading_requirement_container_meta:a4a2ea5e-4a2f-449c-a20b-78144af692d9-1716146482578] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 91a6accc-53b7-4af4-bd65-d1cb0c62494b-1716146482578
  [executable_block:91a6accc-53b7-4af4-bd65-d1cb0c62494b-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_options_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = mid-centered
  x = -102
  y = -30
  width = 98
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 13ff568c-90a4-47c0-af4f-80f7cc39d019-1716146482578
  [loading_requirement_container_meta:13ff568c-90a4-47c0-af4f-80f7cc39d019-1716146482578] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 747bb73d-6209-4bfd-b8b7-0c455f98756b-1716146482578
  [executable_block:747bb73d-6209-4bfd-b8b7-0c455f98756b-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_stats_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = mc_pausescreen_advancements_button
  x = 106
  y = 0
  width = 98
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = c3e9d093-d120-4f6e-8861-a82ff7211c9b-1716146482578
  [loading_requirement_container_meta:c3e9d093-d120-4f6e-8861-a82ff7211c9b-1716146482578] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 5c8bf8ae-30cf-45b8-921b-46e3dcc31bc0-1716146482578
  [executable_block:5c8bf8ae-30cf-45b8-921b-46e3dcc31bc0-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_disconnect_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = mid-centered
  x = -102
  y = 10
  width = 204
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = aad84ed9-c83e-4541-99a8-fff46ee5030c-1716146482578
  [loading_requirement_container_meta:aad84ed9-c83e-4541-99a8-fff46ee5030c-1716146482578] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 6f37420d-623e-47c1-84f2-6781aec3cb09-1716146482578
  [executable_block:6f37420d-623e-47c1-84f2-6781aec3cb09-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_lan_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = mc_pausescreen_options_button
  x = 106
  y = 0
  width = 98
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 50dff7ff-ffc4-4a31-9f1a-49fa417cf1b4-1716146482578
  [loading_requirement_container_meta:50dff7ff-ffc4-4a31-9f1a-49fa417cf1b4-1716146482578] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 377ef66e-635a-4223-ae44-937870d77893-1716146482578
  [executable_block:377ef66e-635a-4223-ae44-937870d77893-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_advancements_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = mc_pausescreen_options_button
  x = 0
  y = -27
  width = 98
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = a9416fa9-fb33-494b-b247-e22626e9d256-1716146482578
  [loading_requirement_container_meta:a9416fa9-fb33-494b-b247-e22626e9d256-1716146482578] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 2b8a9976-5b15-4655-9ccf-fe106a3663e8-1716146482578
  [executable_block:2b8a9976-5b15-4655-9ccf-fe106a3663e8-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_report_bugs_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 431
  y = 176
  width = 98
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 9ea2030f-73e8-48a1-9dca-fc238d768985-1716146482578
  [loading_requirement_container_meta:9ea2030f-73e8-48a1-9dca-fc238d768985-1716146482578] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 1f0340e4-d547-46d0-bb3f-98debc886516-1716146482578
  [executable_block:1f0340e4-d547-46d0-bb3f-98debc886516-1716146482578][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = mc_pausescreen_feedback_button
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 325
  y = 176
  width = 98
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 5f561ecc-1248-4698-859b-309dac58d551-1716146482578
  [loading_requirement_container_meta:5f561ecc-1248-4698-859b-309dac58d551-1716146482578] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

