import mods.jeitweaker.Jei;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.data.IData;
import crafttweaker.api.data.MapData;

// Iron backpack
craftingTable.removeByName("sophisticatedbackpacks:iron_backpack");
craftingTable.addShaped("sophisticatedbackpacks_iron_backpack", <item:sophisticatedbackpacks:iron_backpack>,
  [
    [<item:minecraft:iron_ingot>, <item:minecraft:iron_ingot>, <item:minecraft:iron_ingot>],
    [<item:minecraft:diamond>, <item:sophisticatedbackpacks:backpack>, <item:minecraft:diamond>],
    [<item:minecraft:iron_ingot>, <item:minecraft:iron_ingot>, <item:minecraft:iron_ingot>]
  ],
  (usualOut as IItemStack, inputs as IItemStack[][]) => {
    var tags_data = inputs[1][1].tag;

    if (tags_data != null) {
      var tags = new MapData(tags_data as IData[string]) as MapData;

      return usualOut.withTag({inventorySlots: 36, upgradeSlots: 2, contentsUuid: tags["contentsUuid"]});
    }

    return usualOut;
  });

// Gold backpack
craftingTable.removeByName("sophisticatedbackpacks:gold_backpack");
craftingTable.addShaped("sophisticatedbackpacks_gold_backpack", <item:sophisticatedbackpacks:gold_backpack>,
  [
    [<item:minecraft:gold_ingot>, <item:minecraft:gold_ingot>, <item:minecraft:gold_ingot>],
    [<item:minecraft:netherite_ingot>, <item:sophisticatedbackpacks:iron_backpack>, <item:minecraft:netherite_ingot>],
    [<item:minecraft:gold_ingot>, <item:minecraft:gold_ingot>, <item:minecraft:gold_ingot>]
  ],
  (usualOut as IItemStack, inputs as IItemStack[][]) => {
    var tags_data = inputs[1][1].tag;

    if (tags_data != null) {
      var tags = new MapData(tags_data as IData[string]) as MapData;

      return usualOut.withTag({inventorySlots: 54, upgradeSlots: 3, contentsUuid: tags["contentsUuid"]});
    }

    return usualOut;
  });

// Diamond backpack
craftingTable.removeByName("sophisticatedbackpacks:diamond_backpack");
craftingTable.addShaped("sophisticatedbackpacks_diamond_backpack", <item:sophisticatedbackpacks:diamond_backpack>,
  [
    [<item:minecraft:diamond>, <item:minecraft:diamond>, <item:minecraft:diamond>],
    [<item:undergarden:forgotten_ingot>, <item:sophisticatedbackpacks:gold_backpack>, <item:undergarden:forgotten_ingot>],
    [<item:minecraft:diamond>, <item:minecraft:diamond>, <item:minecraft:diamond>]
  ],
  (usualOut as IItemStack, inputs as IItemStack[][]) => {
    var tags_data = inputs[1][1].tag;

    if (tags_data != null) {
      var tags = new MapData(tags_data as IData[string]) as MapData;

      return usualOut.withTag({inventorySlots: 72, upgradeSlots: 5, contentsUuid: tags["contentsUuid"]});
    }

    return usualOut;
  });

// Remove netherite backpack
recipes.remove(<item:sophisticatedbackpacks:netherite_backpack>);
Jei.hideIngredient(<item:sophisticatedbackpacks:netherite_backpack>);

// Remove copper backpack
recipes.remove(<item:sophisticatedbackpacks:copper_backpack>);
recipes.removeByName("sophisticatedbackpacks:iron_backpack_from_copper");
Jei.hideIngredient(<item:sophisticatedbackpacks:copper_backpack>);
