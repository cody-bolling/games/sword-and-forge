import mods.jeitweaker.Jei;

// Minecraft Ender Eye
craftingTable.removeByName("minecraft:ender_eye");
craftingTable.addShapeless("minecraft_ender_eye", <item:minecraft:ender_eye>, [<item:minecraft:ender_pearl>, <item:twilightforest:fiery_blood>]);

// Minecraft Elytra
craftingTable.addShaped("minecraft_elytra", <item:minecraft:elytra>,
  [
    [<item:minecraft:string>, <item:minecraft:netherite_ingot>, <item:minecraft:string>],
    [<item:minecraft:phantom_membrane>, <item:minecraft:air>, <item:minecraft:phantom_membrane>],
    [<item:minecraft:phantom_membrane>, <item:minecraft:air>, <item:minecraft:phantom_membrane>]
  ]);

// Minecraft Phantom Membrane
craftingTable.addShaped("minecraft_phantom_membrane", <item:minecraft:phantom_membrane>,
  [
    [<item:minecraft:feather>, <item:minecraft:feather>, <item:minecraft:feather>],
    [<item:minecraft:feather>, <item:minecraft:slime_ball>, <item:minecraft:feather>],
    [<item:minecraft:feather>, <item:minecraft:feather>, <item:minecraft:feather>]
  ]);

// Minecraft Flint and Steel
craftingTable.removeByName("minecraft:flint_and_steel");
craftingTable.addShapeless("minecraft_flint_and_steel", <item:minecraft:flint_and_steel>, [<item:minecraft:flint>, <item:alloyed:steel_ingot>]);

// Remove mending enchantment
recipes.remove(<item:minecraft:enchanted_book>.withTag({StoredEnchantments: [{lvl: 1, id: "minecraft:mending"}]}));
Jei.hideIngredient(<item:minecraft:enchanted_book>.withTag({StoredEnchantments: [{lvl: 1, id: "minecraft:mending"}]}));

// Minecraft Name Tag
craftingTable.addShapeless("minecraft_name_tag", <item:minecraft:name_tag>, [<item:minecraft:paper>, <item:minecraft:black_dye>, <item:minecraft:iron_ingot>]);

// Minecraft Bundle
craftingTable.addShapeless("minecraft_bundle", <item:minecraft:bundle>, [<item:minecraft:leather>, <item:minecraft:string>]);
Jei.addIngredient(<item:minecraft:bundle>);
