import mods.jeitweaker.Jei;

// Remove all wooden tools
recipes.remove(<item:minecraft:wooden_sword>);
Jei.hideIngredient(<item:minecraft:wooden_sword>);
recipes.remove(<item:minecraft:wooden_shovel>);
Jei.hideIngredient(<item:minecraft:wooden_shovel>);
recipes.remove(<item:minecraft:wooden_pickaxe>);
Jei.hideIngredient(<item:minecraft:wooden_pickaxe>);
recipes.remove(<item:minecraft:wooden_axe>);
Jei.hideIngredient(<item:minecraft:wooden_axe>);
recipes.remove(<item:minecraft:wooden_hoe>);
Jei.hideIngredient(<item:minecraft:wooden_hoe>);

// Remove all stone tools
recipes.remove(<item:minecraft:stone_sword>);
Jei.hideIngredient(<item:minecraft:stone_sword>);
recipes.remove(<item:minecraft:stone_shovel>);
Jei.hideIngredient(<item:minecraft:stone_shovel>);
recipes.remove(<item:minecraft:stone_pickaxe>);
Jei.hideIngredient(<item:minecraft:stone_pickaxe>);
recipes.remove(<item:minecraft:stone_axe>);
Jei.hideIngredient(<item:minecraft:stone_axe>);
recipes.remove(<item:minecraft:stone_hoe>);
Jei.hideIngredient(<item:minecraft:stone_hoe>);

// Remove all iron tools
recipes.remove(<item:minecraft:iron_sword>);
Jei.hideIngredient(<item:minecraft:iron_sword>);
recipes.remove(<item:minecraft:iron_shovel>);
Jei.hideIngredient(<item:minecraft:iron_shovel>);
recipes.remove(<item:minecraft:iron_pickaxe>);
Jei.hideIngredient(<item:minecraft:iron_pickaxe>);
recipes.remove(<item:minecraft:iron_axe>);
Jei.hideIngredient(<item:minecraft:iron_axe>);
recipes.remove(<item:minecraft:iron_hoe>);
Jei.hideIngredient(<item:minecraft:iron_hoe>);

// Remove all golden tools
recipes.remove(<item:minecraft:golden_sword>);
Jei.hideIngredient(<item:minecraft:golden_sword>);
recipes.remove(<item:minecraft:golden_shovel>);
Jei.hideIngredient(<item:minecraft:golden_shovel>);
recipes.remove(<item:minecraft:golden_pickaxe>);
Jei.hideIngredient(<item:minecraft:golden_pickaxe>);
recipes.remove(<item:minecraft:golden_axe>);
Jei.hideIngredient(<item:minecraft:golden_axe>);
recipes.remove(<item:minecraft:golden_hoe>);
Jei.hideIngredient(<item:minecraft:golden_hoe>);

// Remove all diamond tools
recipes.remove(<item:minecraft:diamond_sword>);
Jei.hideIngredient(<item:minecraft:diamond_sword>);
recipes.remove(<item:minecraft:diamond_shovel>);
Jei.hideIngredient(<item:minecraft:diamond_shovel>);
recipes.remove(<item:minecraft:diamond_pickaxe>);
Jei.hideIngredient(<item:minecraft:diamond_pickaxe>);
recipes.remove(<item:minecraft:diamond_axe>);
Jei.hideIngredient(<item:minecraft:diamond_axe>);
recipes.remove(<item:minecraft:diamond_hoe>);
Jei.hideIngredient(<item:minecraft:diamond_hoe>);

// Remove all netherite tools
recipes.remove(<item:minecraft:netherite_sword>);
Jei.hideIngredient(<item:minecraft:netherite_sword>);
recipes.remove(<item:minecraft:netherite_shovel>);
Jei.hideIngredient(<item:minecraft:netherite_shovel>);
recipes.remove(<item:minecraft:netherite_pickaxe>);
Jei.hideIngredient(<item:minecraft:netherite_pickaxe>);
recipes.remove(<item:minecraft:netherite_axe>);
Jei.hideIngredient(<item:minecraft:netherite_axe>);
recipes.remove(<item:minecraft:netherite_hoe>);
Jei.hideIngredient(<item:minecraft:netherite_hoe>);

// Remove all neptunium tools
recipes.remove(<item:aquaculture:neptunium_sword>);
Jei.hideIngredient(<item:aquaculture:neptunium_sword>);
recipes.remove(<item:aquaculture:neptunium_shovel>);
Jei.hideIngredient(<item:aquaculture:neptunium_shovel>);
recipes.remove(<item:aquaculture:neptunium_pickaxe>);
Jei.hideIngredient(<item:aquaculture:neptunium_pickaxe>);
recipes.remove(<item:aquaculture:neptunium_axe>);
Jei.hideIngredient(<item:aquaculture:neptunium_axe>);
recipes.remove(<item:aquaculture:neptunium_hoe>);
Jei.hideIngredient(<item:aquaculture:neptunium_hoe>);

// Remove all steel tools
recipes.remove(<item:alloyed:steel_sword>);
Jei.hideIngredient(<item:alloyed:steel_sword>);
recipes.remove(<item:alloyed:steel_shovel>);
Jei.hideIngredient(<item:alloyed:steel_shovel>);
recipes.remove(<item:alloyed:steel_pickaxe>);
Jei.hideIngredient(<item:alloyed:steel_pickaxe>);
recipes.remove(<item:alloyed:steel_axe>);
Jei.hideIngredient(<item:alloyed:steel_axe>);
recipes.remove(<item:alloyed:steel_hoe>);
Jei.hideIngredient(<item:alloyed:steel_hoe>);

// Remove all warden tools
recipes.remove(<item:deeperdarker:warden_sword>);
Jei.hideIngredient(<item:deeperdarker:warden_sword>);
recipes.remove(<item:deeperdarker:warden_shovel>);
Jei.hideIngredient(<item:deeperdarker:warden_shovel>);
recipes.remove(<item:deeperdarker:warden_pickaxe>);
Jei.hideIngredient(<item:deeperdarker:warden_pickaxe>);
recipes.remove(<item:deeperdarker:warden_axe>);
Jei.hideIngredient(<item:deeperdarker:warden_axe>);
recipes.remove(<item:deeperdarker:warden_hoe>);
Jei.hideIngredient(<item:deeperdarker:warden_hoe>);

// Remove all crystalline tools
recipes.remove(<item:phantasm:crystalline_sword>);
Jei.hideIngredient(<item:phantasm:crystalline_sword>);
recipes.remove(<item:phantasm:crystalline_shovel>);
Jei.hideIngredient(<item:phantasm:crystalline_shovel>);
recipes.remove(<item:phantasm:crystalline_pickaxe>);
Jei.hideIngredient(<item:phantasm:crystalline_pickaxe>);
recipes.remove(<item:phantasm:crystalline_axe>);
Jei.hideIngredient(<item:phantasm:crystalline_axe>);
recipes.remove(<item:phantasm:crystalline_hoe>);
Jei.hideIngredient(<item:phantasm:crystalline_hoe>);

// Remove all stellium tools
recipes.remove(<item:phantasm:stellium_sword>);
Jei.hideIngredient(<item:phantasm:stellium_sword>);
recipes.remove(<item:phantasm:stellium_shovel>);
Jei.hideIngredient(<item:phantasm:stellium_shovel>);
recipes.remove(<item:phantasm:stellium_pickaxe>);
Jei.hideIngredient(<item:phantasm:stellium_pickaxe>);
recipes.remove(<item:phantasm:stellium_axe>);
Jei.hideIngredient(<item:phantasm:stellium_axe>);
recipes.remove(<item:phantasm:stellium_hoe>);
Jei.hideIngredient(<item:phantasm:stellium_hoe>);

// Remove all tungsten tools
recipes.remove(<item:stalwart_dungeons:tungsten_sword>);
Jei.hideIngredient(<item:stalwart_dungeons:tungsten_sword>);
recipes.remove(<item:stalwart_dungeons:tungsten_shovel>);
Jei.hideIngredient(<item:stalwart_dungeons:tungsten_shovel>);
recipes.remove(<item:stalwart_dungeons:tungsten_pickaxe>);
Jei.hideIngredient(<item:stalwart_dungeons:tungsten_pickaxe>);
recipes.remove(<item:stalwart_dungeons:tungsten_axe>);
Jei.hideIngredient(<item:stalwart_dungeons:tungsten_axe>);
recipes.remove(<item:stalwart_dungeons:tungsten_hoe>);
Jei.hideIngredient(<item:stalwart_dungeons:tungsten_hoe>);

// Remove all chorundum tools
recipes.remove(<item:stalwart_dungeons:chorundum_sword>);
Jei.hideIngredient(<item:stalwart_dungeons:chorundum_sword>);
recipes.remove(<item:stalwart_dungeons:chorundum_shovel>);
Jei.hideIngredient(<item:stalwart_dungeons:chorundum_shovel>);
recipes.remove(<item:stalwart_dungeons:chorundum_pickaxe>);
Jei.hideIngredient(<item:stalwart_dungeons:chorundum_pickaxe>);
recipes.remove(<item:stalwart_dungeons:chorundum_axe>);
Jei.hideIngredient(<item:stalwart_dungeons:chorundum_axe>);
recipes.remove(<item:stalwart_dungeons:chorundum_hoe>);
Jei.hideIngredient(<item:stalwart_dungeons:chorundum_hoe>);

// Remove all skyroot tools
recipes.remove(<item:aether:skyroot_sword>);
Jei.hideIngredient(<item:aether:skyroot_sword>);
recipes.remove(<item:aether:skyroot_shovel>);
Jei.hideIngredient(<item:aether:skyroot_shovel>);
recipes.remove(<item:aether:skyroot_pickaxe>);
Jei.hideIngredient(<item:aether:skyroot_pickaxe>);
recipes.remove(<item:aether:skyroot_axe>);
Jei.hideIngredient(<item:aether:skyroot_axe>);
recipes.remove(<item:aether:skyroot_hoe>);
Jei.hideIngredient(<item:aether:skyroot_hoe>);

// Remove all holystone tools
recipes.remove(<item:aether:holystone_sword>);
Jei.hideIngredient(<item:aether:holystone_sword>);
recipes.remove(<item:aether:holystone_shovel>);
Jei.hideIngredient(<item:aether:holystone_shovel>);
recipes.remove(<item:aether:holystone_pickaxe>);
Jei.hideIngredient(<item:aether:holystone_pickaxe>);
recipes.remove(<item:aether:holystone_axe>);
Jei.hideIngredient(<item:aether:holystone_axe>);
recipes.remove(<item:aether:holystone_hoe>);
Jei.hideIngredient(<item:aether:holystone_hoe>);

// Remove all zanite tools
recipes.remove(<item:aether:zanite_sword>);
Jei.hideIngredient(<item:aether:zanite_sword>);
recipes.remove(<item:aether:zanite_shovel>);
Jei.hideIngredient(<item:aether:zanite_shovel>);
recipes.remove(<item:aether:zanite_pickaxe>);
Jei.hideIngredient(<item:aether:zanite_pickaxe>);
recipes.remove(<item:aether:zanite_axe>);
Jei.hideIngredient(<item:aether:zanite_axe>);
recipes.remove(<item:aether:zanite_hoe>);
Jei.hideIngredient(<item:aether:zanite_hoe>);

// Remove all gravitite tools
recipes.remove(<item:aether:gravitite_sword>);
Jei.hideIngredient(<item:aether:gravitite_sword>);
recipes.remove(<item:aether:gravitite_shovel>);
Jei.hideIngredient(<item:aether:gravitite_shovel>);
recipes.remove(<item:aether:gravitite_pickaxe>);
Jei.hideIngredient(<item:aether:gravitite_pickaxe>);
recipes.remove(<item:aether:gravitite_axe>);
Jei.hideIngredient(<item:aether:gravitite_axe>);
recipes.remove(<item:aether:gravitite_hoe>);
Jei.hideIngredient(<item:aether:gravitite_hoe>);

// Remove all veridium tools
recipes.remove(<item:aether_redux:veridium_sword>);
Jei.hideIngredient(<item:aether_redux:veridium_sword>);
recipes.remove(<item:aether_redux:veridium_shovel>);
Jei.hideIngredient(<item:aether_redux:veridium_shovel>);
recipes.remove(<item:aether_redux:veridium_pickaxe>);
Jei.hideIngredient(<item:aether_redux:veridium_pickaxe>);
recipes.remove(<item:aether_redux:veridium_axe>);
Jei.hideIngredient(<item:aether_redux:veridium_axe>);
recipes.remove(<item:aether_redux:veridium_hoe>);
Jei.hideIngredient(<item:aether_redux:veridium_hoe>);

// Remove all infused_veridium tools
recipes.remove(<item:aether_redux:infused_veridium_sword>);
Jei.hideIngredient(<item:aether_redux:infused_veridium_sword>);
recipes.remove(<item:aether_redux:infused_veridium_shovel>);
Jei.hideIngredient(<item:aether_redux:infused_veridium_shovel>);
recipes.remove(<item:aether_redux:infused_veridium_pickaxe>);
Jei.hideIngredient(<item:aether_redux:infused_veridium_pickaxe>);
recipes.remove(<item:aether_redux:infused_veridium_axe>);
Jei.hideIngredient(<item:aether_redux:infused_veridium_axe>);
recipes.remove(<item:aether_redux:infused_veridium_hoe>);
Jei.hideIngredient(<item:aether_redux:infused_veridium_hoe>);

// Remove all stratus tools
recipes.remove(<item:deep_aether:stratus_sword>);
Jei.hideIngredient(<item:deep_aether:stratus_sword>);
recipes.remove(<item:deep_aether:stratus_shovel>);
Jei.hideIngredient(<item:deep_aether:stratus_shovel>);
recipes.remove(<item:deep_aether:stratus_pickaxe>);
Jei.hideIngredient(<item:deep_aether:stratus_pickaxe>);
recipes.remove(<item:deep_aether:stratus_axe>);
Jei.hideIngredient(<item:deep_aether:stratus_axe>);
recipes.remove(<item:deep_aether:stratus_hoe>);
Jei.hideIngredient(<item:deep_aether:stratus_hoe>);

// Remove all skyjade tools
recipes.remove(<item:deep_aether:skyjade_sword>);
Jei.hideIngredient(<item:deep_aether:skyjade_sword>);
recipes.remove(<item:deep_aether:skyjade_shovel>);
Jei.hideIngredient(<item:deep_aether:skyjade_shovel>);
recipes.remove(<item:deep_aether:skyjade_pickaxe>);
Jei.hideIngredient(<item:deep_aether:skyjade_pickaxe>);
recipes.remove(<item:deep_aether:skyjade_axe>);
Jei.hideIngredient(<item:deep_aether:skyjade_axe>);
recipes.remove(<item:deep_aether:skyjade_hoe>);
Jei.hideIngredient(<item:deep_aether:skyjade_hoe>);

// Remove all ironwood tools
recipes.remove(<item:twilightforest:ironwood_sword>);
Jei.hideIngredient(<item:twilightforest:ironwood_sword>);
recipes.remove(<item:twilightforest:ironwood_shovel>);
Jei.hideIngredient(<item:twilightforest:ironwood_shovel>);
recipes.remove(<item:twilightforest:ironwood_pickaxe>);
Jei.hideIngredient(<item:twilightforest:ironwood_pickaxe>);
recipes.remove(<item:twilightforest:ironwood_axe>);
Jei.hideIngredient(<item:twilightforest:ironwood_axe>);
recipes.remove(<item:twilightforest:ironwood_hoe>);
Jei.hideIngredient(<item:twilightforest:ironwood_hoe>);

// Remove all fiery tools
recipes.remove(<item:twilightforest:fiery_sword>);
Jei.hideIngredient(<item:twilightforest:fiery_sword>);
recipes.remove(<item:twilightforest:fiery_pickaxe>);
Jei.hideIngredient(<item:twilightforest:fiery_pickaxe>);

// Remove all steeleaf tools
recipes.remove(<item:twilightforest:steeleaf_sword>);
Jei.hideIngredient(<item:twilightforest:steeleaf_sword>);
recipes.remove(<item:twilightforest:steeleaf_shovel>);
Jei.hideIngredient(<item:twilightforest:steeleaf_shovel>);
recipes.remove(<item:twilightforest:steeleaf_pickaxe>);
Jei.hideIngredient(<item:twilightforest:steeleaf_pickaxe>);
recipes.remove(<item:twilightforest:steeleaf_axe>);
Jei.hideIngredient(<item:twilightforest:steeleaf_axe>);
recipes.remove(<item:twilightforest:steeleaf_hoe>);
Jei.hideIngredient(<item:twilightforest:steeleaf_hoe>);

// Remove all knightmetal tools
recipes.remove(<item:twilightforest:knightmetal_sword>);
Jei.hideIngredient(<item:twilightforest:knightmetal_sword>);
recipes.remove(<item:twilightforest:knightmetal_pickaxe>);
Jei.hideIngredient(<item:twilightforest:knightmetal_pickaxe>);
recipes.remove(<item:twilightforest:knightmetal_axe>);
Jei.hideIngredient(<item:twilightforest:knightmetal_axe>);

// Remove all giant tools recipes
recipes.remove(<item:twilightforest:giant_sword>);
recipes.remove(<item:twilightforest:giant_pickaxe>);

// Remove all cloggrum tools
recipes.remove(<item:undergarden:cloggrum_sword>);
Jei.hideIngredient(<item:undergarden:cloggrum_sword>);
recipes.remove(<item:undergarden:cloggrum_shovel>);
Jei.hideIngredient(<item:undergarden:cloggrum_shovel>);
recipes.remove(<item:undergarden:cloggrum_pickaxe>);
Jei.hideIngredient(<item:undergarden:cloggrum_pickaxe>);
recipes.remove(<item:undergarden:cloggrum_axe>);
Jei.hideIngredient(<item:undergarden:cloggrum_axe>);
recipes.remove(<item:undergarden:cloggrum_hoe>);
Jei.hideIngredient(<item:undergarden:cloggrum_hoe>);

// Remove all froststeel tools
recipes.remove(<item:undergarden:froststeel_sword>);
Jei.hideIngredient(<item:undergarden:froststeel_sword>);
recipes.remove(<item:undergarden:froststeel_shovel>);
Jei.hideIngredient(<item:undergarden:froststeel_shovel>);
recipes.remove(<item:undergarden:froststeel_pickaxe>);
Jei.hideIngredient(<item:undergarden:froststeel_pickaxe>);
recipes.remove(<item:undergarden:froststeel_axe>);
Jei.hideIngredient(<item:undergarden:froststeel_axe>);
recipes.remove(<item:undergarden:froststeel_hoe>);
Jei.hideIngredient(<item:undergarden:froststeel_hoe>);

// Remove all utherium tools
recipes.remove(<item:undergarden:utherium_sword>);
Jei.hideIngredient(<item:undergarden:utherium_sword>);
recipes.remove(<item:undergarden:utherium_shovel>);
Jei.hideIngredient(<item:undergarden:utherium_shovel>);
recipes.remove(<item:undergarden:utherium_pickaxe>);
Jei.hideIngredient(<item:undergarden:utherium_pickaxe>);
recipes.remove(<item:undergarden:utherium_axe>);
Jei.hideIngredient(<item:undergarden:utherium_axe>);
recipes.remove(<item:undergarden:utherium_hoe>);
Jei.hideIngredient(<item:undergarden:utherium_hoe>);

// Remove all forgotten tools
recipes.remove(<item:undergarden:forgotten_sword>);
Jei.hideIngredient(<item:undergarden:forgotten_sword>);
recipes.remove(<item:undergarden:forgotten_shovel>);
Jei.hideIngredient(<item:undergarden:forgotten_shovel>);
recipes.remove(<item:undergarden:forgotten_pickaxe>);
Jei.hideIngredient(<item:undergarden:forgotten_pickaxe>);
recipes.remove(<item:undergarden:forgotten_axe>);
Jei.hideIngredient(<item:undergarden:forgotten_axe>);
recipes.remove(<item:undergarden:forgotten_hoe>);
Jei.hideIngredient(<item:undergarden:forgotten_hoe>);

// Remove all gold_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:gold_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:gold_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:gold_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:gold_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:gold_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:gold_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:gold_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:gold_upgraded_netherite_axe>);

// Remove all fire_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:fire_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:fire_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:fire_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:fire_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:fire_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:fire_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:fire_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:fire_upgraded_netherite_axe>);

// Remove all ender_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:ender_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:ender_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:ender_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:ender_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:ender_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:ender_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:ender_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:ender_upgraded_netherite_axe>);

// Remove all water_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:water_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:water_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:water_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:water_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:water_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:water_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:water_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:water_upgraded_netherite_axe>);

// Remove all phantom_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:phantom_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:phantom_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:phantom_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:phantom_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:phantom_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:phantom_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:phantom_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:phantom_upgraded_netherite_axe>);

// Remove all feather_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:feather_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:feather_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:feather_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:feather_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:feather_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:feather_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:feather_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:feather_upgraded_netherite_axe>);

// Remove all corrupt_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:corrupt_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:corrupt_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:corrupt_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:corrupt_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:corrupt_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:corrupt_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:corrupt_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:corrupt_upgraded_netherite_axe>);

// Remove all echo_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:echo_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:echo_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:echo_upgraded_netherite_shovel>);
Jei.hideIngredient(<item:upgradednetherite:echo_upgraded_netherite_shovel>);
recipes.remove(<item:upgradednetherite:echo_upgraded_netherite_pickaxe>);
Jei.hideIngredient(<item:upgradednetherite:echo_upgraded_netherite_pickaxe>);
recipes.remove(<item:upgradednetherite:echo_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:echo_upgraded_netherite_axe>);

// Remove all wither_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:wither_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:wither_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:wither_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:wither_upgraded_netherite_axe>);

// Remove all poison_upgraded_netherite tools
recipes.remove(<item:upgradednetherite:poison_upgraded_netherite_sword>);
Jei.hideIngredient(<item:upgradednetherite:poison_upgraded_netherite_sword>);
recipes.remove(<item:upgradednetherite:poison_upgraded_netherite_axe>);
Jei.hideIngredient(<item:upgradednetherite:poison_upgraded_netherite_axe>);

// Remove born_in_chaos_v1 weapons
recipes.remove(<item:born_in_chaos_v1:sharpened_dark_metal_sword>);
Jei.hideIngredient(<item:born_in_chaos_v1:sharpened_dark_metal_sword>);
recipes.remove(<item:born_in_chaos_v1:nightmare_scythe>);
Jei.hideIngredient(<item:born_in_chaos_v1:nightmare_scythe>);
recipes.remove(<item:born_in_chaos_v1:great_reaper_axe>);
Jei.hideIngredient(<item:born_in_chaos_v1:great_reaper_axe>);
recipes.remove(<item:born_in_chaos_v1:darkwarblade>);
Jei.hideIngredient(<item:born_in_chaos_v1:darkwarblade>);
recipes.remove(<item:born_in_chaos_v1:skullbreaker_hammer>);
Jei.hideIngredient(<item:born_in_chaos_v1:skullbreaker_hammer>);
recipes.remove(<item:born_in_chaos_v1:staffof_magic_arrows>);
Jei.hideIngredient(<item:born_in_chaos_v1:staffof_magic_arrows>);
recipes.remove(<item:born_in_chaos_v1:staffofthe_summoner>);
Jei.hideIngredient(<item:born_in_chaos_v1:staffofthe_summoner>);
recipes.remove(<item:born_in_chaos_v1:death_totem>);
Jei.hideIngredient(<item:born_in_chaos_v1:death_totem>);
recipes.remove(<item:born_in_chaos_v1:soul_cutlass>);
Jei.hideIngredient(<item:born_in_chaos_v1:soul_cutlass>);
recipes.remove(<item:born_in_chaos_v1:shell_mace>);
Jei.hideIngredient(<item:born_in_chaos_v1:shell_mace>);
recipes.remove(<item:born_in_chaos_v1:dark_ritual_dagger>);
Jei.hideIngredient(<item:born_in_chaos_v1:dark_ritual_dagger>);
recipes.remove(<item:born_in_chaos_v1:spiritual_sword>);
Jei.hideIngredient(<item:born_in_chaos_v1:spiritual_sword>);
recipes.remove(<item:born_in_chaos_v1:intoxicating_dagger>);
Jei.hideIngredient(<item:born_in_chaos_v1:intoxicating_dagger>);

// Remove stalwart_dungeons hammers
recipes.remove(<item:stalwart_dungeons:tungsten_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:tungsten_hammer>);
recipes.remove(<item:stalwart_dungeons:nether_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:nether_hammer>);
recipes.remove(<item:stalwart_dungeons:wooden_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:wooden_hammer>);
recipes.remove(<item:stalwart_dungeons:stone_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:stone_hammer>);
recipes.remove(<item:stalwart_dungeons:iron_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:iron_hammer>);
recipes.remove(<item:stalwart_dungeons:golden_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:golden_hammer>);
recipes.remove(<item:stalwart_dungeons:diamond_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:diamond_hammer>);
recipes.remove(<item:stalwart_dungeons:netherite_hammer>);
Jei.hideIngredient(<item:stalwart_dungeons:netherite_hammer>);

// Remove dungeons_gear artifacts
recipes.remove(<item:dungeons_gear:powershaker>);
Jei.hideIngredient(<item:dungeons_gear:powershaker>);
recipes.remove(<item:dungeons_gear:corrupted_seeds>);
Jei.hideIngredient(<item:dungeons_gear:corrupted_seeds>);
recipes.remove(<item:dungeons_gear:ice_wand>);
Jei.hideIngredient(<item:dungeons_gear:ice_wand>);
recipes.remove(<item:dungeons_gear:wind_horn>);
Jei.hideIngredient(<item:dungeons_gear:wind_horn>);
recipes.remove(<item:dungeons_gear:satchel_of_elements>);
Jei.hideIngredient(<item:dungeons_gear:satchel_of_elements>);
recipes.remove(<item:dungeons_gear:updraft_tome>);
Jei.hideIngredient(<item:dungeons_gear:updraft_tome>);
recipes.remove(<item:dungeons_gear:gong_of_weakening>);
Jei.hideIngredient(<item:dungeons_gear:gong_of_weakening>);
recipes.remove(<item:dungeons_gear:soul_healer>);
Jei.hideIngredient(<item:dungeons_gear:soul_healer>);
recipes.remove(<item:dungeons_gear:totem_of_regeneration>);
Jei.hideIngredient(<item:dungeons_gear:totem_of_regeneration>);
recipes.remove(<item:dungeons_gear:totem_of_shielding>);
Jei.hideIngredient(<item:dungeons_gear:totem_of_shielding>);
recipes.remove(<item:dungeons_gear:totem_of_soul_protection>);
Jei.hideIngredient(<item:dungeons_gear:totem_of_soul_protection>);
recipes.remove(<item:dungeons_gear:corrupted_beacon>);
Jei.hideIngredient(<item:dungeons_gear:corrupted_beacon>);
recipes.remove(<item:dungeons_gear:harpoon_quiver>);
Jei.hideIngredient(<item:dungeons_gear:harpoon_quiver>);
recipes.remove(<item:dungeons_gear:satchel_of_elixirs>);
Jei.hideIngredient(<item:dungeons_gear:satchel_of_elixirs>);
recipes.remove(<item:dungeons_gear:satchel_of_snacks>);
Jei.hideIngredient(<item:dungeons_gear:satchel_of_snacks>);
recipes.remove(<item:dungeons_gear:eye_of_the_guardian>);
Jei.hideIngredient(<item:dungeons_gear:eye_of_the_guardian>);
recipes.remove(<item:dungeons_gear:boots_of_swiftness>);
Jei.hideIngredient(<item:dungeons_gear:boots_of_swiftness>);
recipes.remove(<item:dungeons_gear:death_cap_mushroom>);
Jei.hideIngredient(<item:dungeons_gear:death_cap_mushroom>);
recipes.remove(<item:dungeons_gear:golem_kit>);
Jei.hideIngredient(<item:dungeons_gear:golem_kit>);
recipes.remove(<item:dungeons_gear:tasty_bone>);
Jei.hideIngredient(<item:dungeons_gear:tasty_bone>);
recipes.remove(<item:dungeons_gear:wonderful_wheat>);
Jei.hideIngredient(<item:dungeons_gear:wonderful_wheat>);
recipes.remove(<item:dungeons_gear:lightning_rod>);
Jei.hideIngredient(<item:dungeons_gear:lightning_rod>);
recipes.remove(<item:dungeons_gear:iron_hide_amulet>);
Jei.hideIngredient(<item:dungeons_gear:iron_hide_amulet>);
recipes.remove(<item:dungeons_gear:love_medallion>);
Jei.hideIngredient(<item:dungeons_gear:love_medallion>);
recipes.remove(<item:dungeons_gear:ghost_cloak>);
Jei.hideIngredient(<item:dungeons_gear:ghost_cloak>);
recipes.remove(<item:dungeons_gear:harvester>);
Jei.hideIngredient(<item:dungeons_gear:harvester>);
recipes.remove(<item:dungeons_gear:shock_powder>);
Jei.hideIngredient(<item:dungeons_gear:shock_powder>);
recipes.remove(<item:dungeons_gear:light_feather>);
Jei.hideIngredient(<item:dungeons_gear:light_feather>);
recipes.remove(<item:dungeons_gear:flaming_quiver>);
Jei.hideIngredient(<item:dungeons_gear:flaming_quiver>);
recipes.remove(<item:dungeons_gear:torment_quiver>);
Jei.hideIngredient(<item:dungeons_gear:torment_quiver>);
recipes.remove(<item:dungeons_gear:buzzy_nest>);
Jei.hideIngredient(<item:dungeons_gear:buzzy_nest>);
recipes.remove(<item:dungeons_gear:enchanted_grass>);
Jei.hideIngredient(<item:dungeons_gear:enchanted_grass>);
recipes.remove(<item:dungeons_gear:corrupted_pumpkin>);
Jei.hideIngredient(<item:dungeons_gear:corrupted_pumpkin>);
recipes.remove(<item:dungeons_gear:thundering_quiver>);
Jei.hideIngredient(<item:dungeons_gear:thundering_quiver>);
recipes.remove(<item:dungeons_gear:soul_lantern>);
Jei.hideIngredient(<item:dungeons_gear:soul_lantern>);
recipes.remove(<item:dungeons_gear:fireworks_display>);
Jei.hideIngredient(<item:dungeons_gear:fireworks_display>);

// Remove dungeons_mobs artifacts
recipes.remove(<item:dungeons_mobs:windcaller_staff>);
Jei.hideIngredient(<item:dungeons_mobs:windcaller_staff>);
recipes.remove(<item:dungeons_mobs:geomancer_staff>);
Jei.hideIngredient(<item:dungeons_mobs:geomancer_staff>);
recipes.remove(<item:dungeons_mobs:necromancer_staff>);
Jei.hideIngredient(<item:dungeons_mobs:necromancer_staff>);
recipes.remove(<item:dungeons_mobs:necromancer_trident>);
Jei.hideIngredient(<item:dungeons_mobs:necromancer_trident>);

// Misc Removals
recipes.remove(<item:cnb:cinder_sword>);
Jei.hideIngredient(<item:cnb:cinder_sword>);
recipes.remove(<item:stalwart_dungeons:awful_dagger>);
Jei.hideIngredient(<item:stalwart_dungeons:awful_dagger>);
recipes.remove(<item:unusualend:moult_dagger>);
Jei.hideIngredient(<item:unusualend:moult_dagger>);
recipes.remove(<item:unusualend:shiny_dagger>);
Jei.hideIngredient(<item:unusualend:shiny_dagger>);
