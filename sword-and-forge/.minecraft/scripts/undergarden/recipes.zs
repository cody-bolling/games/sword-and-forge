// Undergarden catalyst
craftingTable.removeByName("undergarden:catalyst");

craftingTable.addShaped("undergarden_catalyst", <item:undergarden:catalyst>,
  [
    [<item:minecraft:gold_ingot>, <item:minecraft:netherite_ingot>, <item:minecraft:gold_ingot>],
    [<item:minecraft:netherite_ingot>, <item:minecraft:nether_star>, <item:minecraft:netherite_ingot>],
    [<item:minecraft:gold_ingot>, <item:minecraft:netherite_ingot>, <item:minecraft:gold_ingot>]
  ]);
