// Aether portal frame block
craftingTable.addShaped("aether_portal_frame_block", <item:aether:aether_portal_frame_block>,
  [
    [<item:endlessbiomes:encrusted_arkan>, <item:endlessbiomes:encrusted_arkan>, <item:endlessbiomes:encrusted_arkan>],
    [<item:endlessbiomes:anklor_shell_fragments>, <item:phantasm:fallen_star>, <item:endlessbiomes:anklor_shell_fragments>],
    [<item:endlessbiomes:encased_radon>, <item:endlessbiomes:reaching_marula>, <item:endlessbiomes:encased_radon>]
  ]);
