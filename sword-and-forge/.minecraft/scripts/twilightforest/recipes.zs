// Twilight Forest Fiery Blood
<recipetype:create:mixing>.addRecipe("fiery_blood_multiplication",
  <constant:create:heat_condition:heated>,
  [
    <item:twilightforest:fiery_blood> * 4
  ],
  [
    <item:twilightforest:fiery_blood>,
    <item:undergarden:regalium_crystal>,
    <item:undergarden:utherium_crystal>,
    <item:undergarden:ink_mushroom>,
    <item:undergarden:blood_mushroom>,
    <item:undergarden:veil_mushroom>,
    <item:undergarden:indigo_mushroom>
  ],
  [
    <fluid:undergarden:virulent_mix_source> * 250
  ], 100);
