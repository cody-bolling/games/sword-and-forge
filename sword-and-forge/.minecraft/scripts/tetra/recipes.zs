import crafttweaker.api.events.CTEventManager;
import crafttweaker.api.event.entity.player.interact.RightClickBlockEvent;
import crafttweaker.api.util.InteractionHand;
import mods.jeitweaker.Jei;

CTEventManager.register<RightClickBlockEvent>((event) => {
  val player = event.getPlayer();
  val item = player.inventory.getSelected().asIItemStack();
  val tags = item.tag;

  if (tags != null && ("basic_hammer_right" in tags.asString() || "basic_hammer_left" in tags.asString()) && item.amount > 0)
  {
    val level = player.getLevel();
    val blockState = level.getBlockState(event.getBlockPos());
    val blockID = blockState.asString();
    val tetraTableID = "Block{tetra_tables:tetra_table_";

    if (tetraTableID in blockID)
    {
      val startIndex = tetraTableID.length;
      val endIndex = blockID.length - 1;
      var woodID = "";

      for i in startIndex .. endIndex
      {
          woodID += blockID[i];
      }

      level.setBlockAndUpdate(event.getBlockPos(), <blockstate:workshop_for_handsome_adventurer:simple_table_${woodID}>);
    }
  }
});

// Remove tetra workbench as they've been replaced by the mod Workshop for Handsome Adventurer
recipes.remove(<item:tetra:basic_workbench>);
Jei.hideIngredient(<item:tetra:basic_workbench>);
