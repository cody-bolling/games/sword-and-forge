// Create Crushed Raw Iron
<recipetype:create:crushing>.removeByInput(<item:minecraft:raw_iron>);
<recipetype:create:crushing>.addRecipe("crushed_raw_iron", [<item:create:crushed_raw_iron>, <item:create:crushed_raw_iron> % 35, <item:create:experience_nugget> % 35], <item:minecraft:raw_iron>, 200);

// Create Crushed Raw Gold
<recipetype:create:crushing>.removeByInput(<item:minecraft:raw_gold>);
<recipetype:create:crushing>.addRecipe("crushed_raw_gold", [<item:create:crushed_raw_gold>, <item:create:crushed_raw_gold> % 35, <item:create:experience_nugget> % 35], <item:minecraft:raw_gold>, 200);

// Create Crushed Raw Copper
<recipetype:create:crushing>.removeByInput(<item:minecraft:raw_copper>);
<recipetype:create:crushing>.addRecipe("crushed_raw_copper", [<item:create:crushed_raw_copper>, <item:create:crushed_raw_copper> % 35, <item:create:experience_nugget> % 35], <item:minecraft:raw_copper>, 200);

<recipetype:create:crushing>.removeByInput(<item:minecraft:copper_ore>);
<recipetype:create:crushing>.addRecipe("crushed_copper_ore", [<item:create:crushed_raw_copper>, <item:create:crushed_raw_copper> % 75, <item:create:experience_nugget> * 2 % 75, <item:minecraft:cobblestone> % 12], <item:minecraft:copper_ore>, 200);

<recipetype:create:crushing>.removeByInput(<item:minecraft:deepslate_copper_ore>);
<recipetype:create:crushing>.addRecipe("crushed_deepslate_copper_ore", [<item:create:crushed_raw_copper> * 2, <item:create:crushed_raw_copper> % 25, <item:create:experience_nugget> * 2 % 75, <item:minecraft:deepslate_copper_ore> % 12], <item:minecraft:deepslate_copper_ore>, 200);

// Create Crushed Raw Zinc
<recipetype:create:crushing>.removeByInput(<item:create:raw_zinc>);
<recipetype:create:crushing>.addRecipe("crushed_raw_zinc", [<item:create:crushed_raw_zinc>, <item:create:crushed_raw_zinc> % 35, <item:create:experience_nugget> % 35], <item:create:raw_zinc>, 200);

// Create Steel Nugget Alternative
craftingTable.addShaped("create_steel_nugget", <item:alloyed:steel_nugget>,
  [
    [<item:minecraft:air>, <item:create:iron_sheet>, <item:minecraft:air>],
    [<item:create:iron_sheet>, <item:minecraft:coal>, <item:create:iron_sheet>],
    [<item:minecraft:air>, <item:create:iron_sheet>, <item:minecraft:air>]
  ]);

// Tetra Pristine Items
<recipetype:create:compacting>.addRecipe("compacted_pristine_diamond", <constant:create:heat_condition:none>, [<item:tetra:pristine_diamond> % 100], [<item:minecraft:diamond> * 4], [<fluid:create_enchantment_industry:experience> * 100], 200);
<recipetype:create:compacting>.addRecipe("compacted_pristine_emerald", <constant:create:heat_condition:none>, [<item:tetra:pristine_emerald> % 100], [<item:minecraft:emerald> * 4], [<fluid:create_enchantment_industry:experience> * 100], 200);
<recipetype:create:compacting>.addRecipe("compacted_pristine_lapis", <constant:create:heat_condition:none>, [<item:tetra:pristine_lapis> % 100], [<item:minecraft:lapis_lazuli> * 4], [<fluid:create_enchantment_industry:experience> * 100], 200);

// Update Extended Cogwheels Recipes
recipes.remove(<item:extendedgears:half_shaft_cogwheel>);
recipes.remove(<item:extendedgears:large_half_shaft_cogwheel>);
recipes.remove(<item:extendedgears:shaftless_cogwheel>);
recipes.remove(<item:extendedgears:large_shaftless_cogwheel>);

craftingTable.addShapeless("custom_extendedgears_half_shaft_cogwheel", <item:extendedgears:half_shaft_cogwheel>, [<item:create:cogwheel>, <item:create:wrench>.reuse()]);
craftingTable.addShapeless("custom_extendedgears_large_half_shaft_cogwheel", <item:extendedgears:large_half_shaft_cogwheel>, [<item:create:large_cogwheel>, <item:create:wrench>.reuse()]);
craftingTable.addShapeless("custom_extendedgears_shaftless_cogwheel", <item:extendedgears:shaftless_cogwheel>, [<item:extendedgears:half_shaft_cogwheel>, <item:create:wrench>.reuse()]);
craftingTable.addShapeless("custom_extendedgears_large_shaftless_cogwheel", <item:extendedgears:large_shaftless_cogwheel>, [<item:extendedgears:large_half_shaft_cogwheel>, <item:create:wrench>.reuse()]);