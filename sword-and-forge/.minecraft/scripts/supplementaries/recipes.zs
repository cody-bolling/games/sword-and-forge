// Aether portal frame block
smithing.removeByName("supplementaries:safe");
craftingTable.addShaped("supplementaries_safe", <item:supplementaries:safe>,
  [
    [<item:minecraft:iron_ingot>, <item:minecraft:netherite_ingot>, <item:minecraft:iron_ingot>],
    [<item:minecraft:iron_ingot>, <item:minecraft:chest>, <item:minecraft:iron_ingot>],
    [<item:minecraft:iron_ingot>, <item:minecraft:iron_block>, <item:minecraft:iron_ingot>]
  ]);
