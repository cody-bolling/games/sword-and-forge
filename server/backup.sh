#!/bin/sh

echo "[INFO] Starting backup..."
pushd /minecraft/server
zip -r world-backup.zip world/*
popd

aws s3api head-object --bucket "sword-and-forge-server" --key world-backup.zip || not_exist=true

if ! [[ $not_exist ]]; then
  aws s3api delete-object --bucket "sword-and-forge-server" --key world-backup.zip
fi

aws s3api put-object --bucket "sword-and-forge-server" --key world-backup.zip --body /minecraft/server/world-backup.zip
rm -f /minecraft/server/world-backup.zip
echo "[INFO] Backup done!"
