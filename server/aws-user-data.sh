#!/bin/bash
echo "[INFO] Install java 17 and cronie..."
sudo yum -y install java-17
sudo yum -y install cronie

echo "[INFO] Create minecraft directory..."
sudo mkdir /minecraft
cd /minecraft

echo "[INFO] Download server files..."
aws s3 cp s3://sword-and-forge-server/server.zip /minecraft/server.zip
unzip /minecraft/server.zip
sudo chmod 777 /minecraft/server/run.sh
sudo rm -f /minecraft/server.zip

echo "[INFO] Download world backup if need be..."
aws s3api head-object --bucket "sword-and-forge-server" --key world-backup.zip || not_exist=true
if [ -z $not_exist ] && [ ! -d "/minecraft/server/world" ]; then
  pushd /minecraft/server
  aws s3 cp s3://sword-and-forge-server/world-backup.zip /minecraft/server/world-backup.zip
  unzip /minecraft/server/world-backup.zip
  sudo rm -f /minecraft/server/world-backup.zip
  popd
fi

echo "[INFO] Setup backup crontab..."
aws s3 cp s3://sword-and-forge-server/backup.sh /minecraft/backup.sh
sudo chmod 777 /minecraft/backup.sh
echo "*/20 * * * * /bin/sh -c '/minecraft/backup.sh'" | sudo tee /minecraft/backup-cron > /dev/null
sudo crontab /minecraft/backup-cron
sudo systemctl daemon-reload
sudo systemctl enable crond.service
sudo systemctl start crond.service

echo "[INFO] Set ec2-user as owner for everything under /minecraft"
sudo chown -R ec2-user:ec2-user /minecraft
sudo chmod 775 /run/screen

echo "[INFO] Setup and start minecraft.service..."
sudo aws s3 cp s3://sword-and-forge-server/minecraft.service /etc/systemd/system/minecraft.service
sudo systemctl daemon-reload
sudo systemctl enable minecraft.service
sudo systemctl start minecraft.service
