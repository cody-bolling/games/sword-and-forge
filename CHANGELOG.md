# CHANGELOG

## 2.7.4

### Added

- add vanilla tweaks silence mobs data pack

### Changed

- disable flutter spawns because their flapping is annoying
- update crushed copper recipes to be more consistent with other vanilla ores
- update recipe for supplementaries safe to make it easier to get

## 2.7.3

### Changed

- update extended cogwheel half shaft and shaftless recipes to be less annoying and just use the create wrench

## 2.7.2

## Removed

- remove mod enhanced block entities
  - was causing an annoying z-fighting visual glitch because it was conflicting with flywheel which also optimizes entities

## 2.7.1

## Added

- add recipe to make use of environmental cattails
- add mod bad horse fix
  - fixes graphical bug caused by oculus (shader mod) and horse armor
- add mod enchantment descriptions

## Changed

- update pipeline to use the pipeline manager release pipeline
- update steel and bronze tool levels to diamond instead of iron for tetra

## Fixed

- fix sophisticated backpack inventory sizes
  - when they are crafted with the custom crafttweaker recipes they have proper inventory sizes now
- copy patchouli book files to the server archive

## Removed

- remove mod simple backups as it is not necessary for our server setup

## 2.7.0

## Added

- add devcontainer
- add custom patchouli book "Sword and Forge Guide"
  - add book to players on first spawn
  - add crafting recipe for the guide book
  - add notable mods list
- add recipe for bundle and show it in JEI
- add lewis server by default
- add mods
  - async locator
    - this mod puts the locate function thread on an asynchronous thread so generating buried treasure maps doesn't crash the game
  - ender dragon fight remastered
  - journey map
  - no telemetry
  - oculus geckolib compat
  - oculus particle fix

## Changed

- fix pipeline
- fix server required files to match updated mods
- make borderless the default
- disable narrator by default
- make patchouli book large by default
- remove aquaculture fish weights so they stack better
- disable cape by default
- update default voice chat key to v
- move chunky to modpack instead of just server
- update complimentary shaders to 5.4
- update forge to version 43.4.16
- mod updates
  - aether redux 2.0.16 > 2.0.17
  - oculus 1.6.9 > 1.6.9a
  - oculus flywheel compat 0.2.1 > 1.0.3b

## Fixed

- fixed tetra table cherry texture being broken
- fix copper ore not dropping anything

## Removed

- remove xaeros minimap, world map, and compat mods
- advancement plaques
- infinite music
- loot journal
- equipment compare

## 2.6.0

- update menus
- increase entity max vertical render distance from 32 to 64 blocks
- increase infinite music timer from 60 to 90 seconds
- update complementary shaders 5.0.1 -> 5.1.1
- increase shader default BLOCK_REFLECT_QUALITY from 1 to 2
- remove all remaining unwanted tools and weapons using saf script
- restructure datapacks, resourcepacks, and shaderpacks into the resources directory
- add scripts for archiving and deploying the modpack
  - udpate pipeline to use these scripts
- add script to generate tetra materials
- add modded plants and leaves to the shader so they wave
- nerf tetra metal materials
  - after some improvements they were just too broken
- update forge to 43.3.13
- update default options
- update aether portal frame block texture and added shader support for it
- update aether portal frame block recipe in order to encourage more exploration of the end
- add recipe for name tags
- add recipe to multiply fiery blood from the twilight forest
  - this prevents unwanted repeats of the hydra fight and encourages more exploration of the undergarden
- remove all vanilla wooden tools
- nerf copper ore so it only drops one raw copper like all other ores
- add mods
  - aether delight 1.0.0
  - alexs delight 1.0.0
  - aquaculture delight 1.0.0
  - astikor carts 1.1.2
    - update carryon config file to blacklist astikor carts
  - better statistics screen 2.2.2
  - bridging mod 2.1.1
  - chunky 1.3.38
    - only to server
  - crafting tweaks 15.1.9
  - create: central kitchen 1.3.11.c
  - create: diesel generators 1.2h
  - create: mechanical spawner 0.0.14.e-44
  - create: new age 1.1.2
    - add library support mod botarium 1.9.2
  - create: slice and dice 2.3.2
  - create: structures 0.1.0
  - create: tipsy n dizzy 0.1
  - curious lanterns 1.3.3 with custom patch
  - deep aether 1.0.2
    - add leaf fluff for newly added leaf types
    - remove deep aether tools
  - drippy loading screen 3.0.1
  - dynamic asset generator 1.2.0
  - dynamic fps 3.4.3
  - embeddium 0.3.18
  - embeddium dynamic lights 1.4.0
  - embeddium++ 1.2.13
  - excavated variants 1.0.3
  - global gamerules 7.0.1.7
  - incontrol 7.1.14
  - just zoom 2.0.0
  - login protection 1.9
  - melody 1.0.1
  - mowzies mobs 1.6.3
  - my server is compatible 1.0
  - nether portal fix 10.0.1
  - nethers delight 3.1
  - radiant gear 2.0.4
  - simple backups 2.1.10
  - skin layers 3d 1.6.4
  - sparse structures 1.0.0
  - tetra tables looking neat 1.0.2
  - undergarden delight
  - workshop for handsome adventurer 1.15.0
- remove mods
  - create broken bad
  - BetterVillages_MoreVillagers compat data pack
    - was causing a crash in Issue #31
    - compat seems to exist without it so likely was never needed in the first place
  - enchantment descriptions
  - macaws bridges
  - macaws fences
  - mimic
    - wasn't spawning anywhere
  - more villagers
  - rubidium
    - replaced with embeddium
  - rubidium extras
    - replaced with embeddium++
  - structory towers
    - was causing a crash in Issue #31
  - trashcans
  - visual workbench
    - functionality replaced by workshop for handsome adventurer
  - yungs extras
- update mods
  - aeroblender 1.0 -> 1.0.1
  - aether 1.0.0 -> 1.4.2
  - aether redux 1.3.3 -> 2.0.16
  - aquaculture 2.4.8 -> 2.4.17
  - architectury 6.5.85 -> 6.6.92
  - attribute fix 17.2.7 -> 17.2.8
  - born in chaos 1.9 -> 1.10.1
    - remove newly added weapons
  - canary 0.2.8 -> 0.3.3
  - carryon 2.1.1.22 -> 2.1.2.23
  - cloth config 8.3.103 -> 8.3.115
  - code chicken lib 4.3.1.481 -> 4.3.2.490
  - corpse 1.0.0 -> 1.0.12
  - craft tweaker 10.1.52 -> 10.1.55
  - create broken bad 2.3.3 -> 2.4.1
  - create encased 1.5.0-ht2 -> 1.6.0-ht1
  - create enchantment industry 1.2.7.d -> 1.2.9.e
  - create extended gears 2.1.0 -> 2.1.1
  - create jetpack 3.2.3 -> 3.3.1
  - create steam and rails 1.5.3 -> 1.6.4
  - cupboard 2.1 -> 2.6
  - curios 5.1.4.3 -> 5.1.6.2
  - dungeons gear 5.0.5 -> 5.0.6
    - remove fireworks display artifact
  - dugeons libraries 3.0.10 -> 3.0.12
  - environmental 3.0.0 -> 3.1.1
    - add leaf fluff for newly added leaf types
  - every wood compat 2.5.14 -> 2.5.22
  - explorify 1.3.0 -> 1.4.0
  - exposure 1.1.0 -> 1.6.0
    - update new ui to be dark mode
  - fancy menu 2.14.9 -> 3.2.3
  - farmers delight 1.2.3 -> 1.2.4
  - fast paintings 1.1.2 -> 1.1.3
  - functional storage 1.1.9 -> 1.1.10
  - fusion 1.1.0 -> 1.1.1
  - guard villagers 1.5.8 -> 1.5.9
  - inventory profile next 1.10.9 -> 1.10.10
  - jade 8.9.1 -> 8.9.2
  - jei 11.6.0.1018 -> 11.6.0.1019
  - konkrete 1.6.1 -> 1.8.0
  - l_enders cataclysm 1.38 -> 1.99.2
    - add mod lionfish api 1.8
    - add mod archaeology api 1.0.0
  - lib ipn 4.0.1 -> 4.0.2
  - loot journal 2.1 -> 3
  - moonlight 2.3.5 -> 2.3.6
  - more mob variants 1.2.0 -> 1.3.0.1
  - mutil 5.1.0 -> 5.2.0
  - not enough animations 1.6.2 -> 1.7.3
  - placebo 7.3.4 -> 7.4.0
  - polymorph 0.46.5 -> 0.46.6
  - rechiseled 1.1.5c -> 1.1.6
  - rechiseled create 1.0.1 -> 1.0.2
  - redirector 3.4.0 -> 5.0.0
  - saturn 0.0.7 -> 0.1.4
  - shield expansion 1.1.7 -> 1.1.7a
  - smooth boot 0.0.2 -> 0.0.4
  - sophisticated backpacks 3.18.66.951 -> 3.20.2.1035
    - remove copper backpack recipe and item
  - sophisticated core 0.5.105.498 -> 0.6.4.605
  - structure compass 1.4.6 -> 1.4.7
  - supermartinijn642 core lib 1.1.15 -> 1.1.17
  - supplementaries 2.4.11 -> 2.4.20
  - tetra 5.5.1 -> 5.6.0
  - titanium 3.7.4-28 -> 3.7.4-33
  - voicechat 2.4.28 -> 2.5.14
  - waystones 11.4.1 -> 11.4.2
  - xaeros minimap 23.8.4 -> 24.1.1
  - xaeros worldmap 1.36.0 -> 1.38.4
- add shaders vector and epoch

## 2.5

- remove dungeons_gear artifacts recipes and from jei
- update all loot tables to remove all tools and artifacts
- update config
  - aether
  - create
  - curios
  - jade
  - player revive
  - right click harvest
  - sophisticated backpacks
  - voice chat
- update mods
  - aether redux
  - betterf3
  - born in chaos
  - carry on
  - chunk sending
  - craft tweaker
  - create
  - create broken bad
  - create casing
  - create enchantment industry
  - create liquid burner
  - create steam and rails
  - curios
  - deep aether
  - every comp
  - fusion
  - jade addons
  - jer integration
  - libipn
  - loot integrations
  - moonlight lib
  - placebo lib
  - rechiseled
  - shield expansion
  - smooth chunk
  - sophisticated backpacks
  - sophisticated core
  - supplementaries
  - yungs api
  - yungs better mineshafts
- update default options
- add mods
  - exposure
  - farmers delight
  - right click harvest
- remove mods
  - deep aether
  - medieval music
    - was too repetitive and replaced normal minecraft music, will try and find a workaround later
- add all modded leaves to have better foliage fluff
- reduce corpse fish spawn rate
- update dark ui resource pack
- update pipeline jobs and server files
- add jei related mods to server files so recipes can be removed properly
- remove tools from dungeon crawl treasure loot tables
- disable all exclusive structures from towns and towers as some were causing crashes
- update steel nugget recipe to be obtainable before nether
- remove fire charges from some loot tables
- update sophisticated backpack recipes so an upgrade keeps the NBT tags and inventory
- increase music volume from 5% to 10%
- update xaeros minimap default time display
- nerf seagulls
- reduce ocean mob spawns
- add exposure mod gui textures to dark ui resource pack
- remove custom exposure recipes from create automation
- fix curios texture paths in dark ui resource pack
- set correct number of curios slots

## 2.4

- update configs
  - betterf3
  - easy anvils
  - effective
  - fancy menu
  - infernal mobs
  - infinite music
  - inventory profile next
  - jade
  - sophisticated backpacks
  - xaeros minimap
- reduce sunken skeleton spawning
- lower infernal mob chance
- add vanilla tweaks data pack and resource pack
  - add some new 3d items like ladders
  - removes enderman griefing and phantoms
- move dark ui textures in the saf resource pack to a separate resource pack dark ui
- add custom main menu
- lower tool requirements for some overworld metals
- add mods
  - legendary tooltips
  - prism
  - simple voice chat
- add recipes for tetra pristine materials
- add ability to repair all dungeons gear with tetra pristine emeralds
- fix geckolib render bug
- remove mending
- remove mod smooth swapping
- update tetra workbench to drop the workbench instead of a crafting table
- update sophisticated backpack recipes to make them more difficult
- disable twilight forest uncrafting table
- remove most tools/weapons recipes and from jei
- reduce chance of getting giant tools from twilight forest giants

## 2.3

- update forge version to 43.3.5
- update complementary shaders to 5.0.1
- remove mod universal enchants as it is not compatible with tetra
- remove mod powder power as it is too difficult to balance
- add safpatch mod and update tetra materials
- add updated config files
- add mod alexs mobs
- remove mod enemy expansion
- add resource pack icon xaeros

## 2.2

- rename darkmode resource pack to sword and forge and update undergarden catalyst texture
- remove ctov and terralith tweaks from sword-and-forge data pack
- update mod the aether to use custom version
- update multimc instance icon to enderman
- add updated ruined_portal loot table to SAF data pack that removes flint and steel
- update ender eye and related textures in SAF resource pack
- update undergarden catalyst texture in SAF resource pack
- add mods
  - amplified nether
  - better foliage renewed
  - chunk sending
  - create encased
  - create extended cogwheels
  - createtweaker
  - easy magic
  - endless biomes
  - ends phantasm
  - entity culling
  - fancy menu
  - fix gpu memory leak
  - jei tweaker
  - koncrete
  - lightspeed
  - loot journal
  - oculus flywheel compat
  - overflowing bars
  - patchouli
  - redirector
  - smooth chunk save
  - unusual end
- remove mods
  - falling leaves (replaced with better foliage)
  - immersive armors
  - nullscape (replaced with new end generation mods)
  - pickup notifier (replaced with loot journal)
  - tetranomicon
  - undergarden tetra patch
- update crafttweaker scripts
  - alternative recipe for create crushed raw ore output
  - alternative recipe for create steel nugget
  - new recipe for aether portal frame blocks
  - new recipe for minecraft elytra
  - new recipe for minecraft phantom membrane
  - new recipe for minecraft flint and steel
  - new recipe for minecraft ender eye
  - new recipe for undergarden catalyst
- add initial settings for all tetra materials
- add tetra materials for mods
  - aether
  - aquaculture
  - born in chaos
  - create
  - create: alloyed
  - powder power
  - stalwart dungeons
  - twilight forest
  - undergarden

## 2.1

- remove mod ctov (caused crashes)
- remove mod entity culling (function done by rubidium extras mod)
- remove mod enchanting infuser (create: enchantment industry does this is a way)
- remove mod shutup experimental settings (did not work, replacing with yeetus experimentus)
- add mod yeetus experimentus
- add mod jer integration
- remove mod when dungeons arise
- remove mod the graveyard
- add mod smooth swapping

## 2.0

- remove mods
  - mob grinding utils
  - mowzies mobs
  - agricraft
  - tinkers construct
  - tetra net fix
  - cowboy up
  - item chat
  - weapon leveling
  - advancements tracker
  - configured
- remove old configs
- update minecraft, lwjgl, and forge versions
- cleanup library mods
- remove 1.18.2 mods
- enable git lfs and add all new 1.19.2 mods to be tracked by lfs
- add mods
  - big tech
    - create: alloyed
    - create: broken bad
    - create: chunkloading
    - create: crystal clear
    - create: enchantment industry
    - create: jetpack
    - create: liquid burner
  - bug fixes
    - no chat reports
  - content addons
    - environmental
    - savage and ravage
    - the graveyard
    - upgrade aquatic
  - decoration
    - rechiseled: create
    - supplementaries
  - dimensions
    - aether
    - aether redux
    - deep aether
    - deeper darker
    - undergarden
  - dungeons
    - l_ender's cataclysm
    - when dungeons arise
  - mobs
    - enemy expansion
    - creatures and beasts
    - mimic
    - nether skeletons
  - performance
    - entity collision fps fix
    - fast paintings
    - let me despawn
    - rubidium extras
  - qol
    - boatload
    - carry on
    - easy anvils
    - easy shulker boxes
    - enchanting infuser
    - player revive
    - structure compass
    - trade cycling
    - universal enchants
    - visual workbench
  - storage
    - functional storage
  - visual
    - more axolotl variants
    - more mob variants
    - public gui announcement
  - ui
    - equipment compare
    - pickup notifier
    - showcase item
    - stylish effects
  - weapons, armor, and combat
    - powder power
    - shield expansion
    - undergarden tetra patch
    - upgraded netherite
  - world generation and structures
    - better villages
    - explorify
    - structory
    - structory towers
    - yung's better end island
    - yung's better jungle temple
    - yung's better nether fortress
  - misc
    - craft tweaker
    - loot integrations
  - libraries
    - aeroblender
    - blueprint
    - citadel
    - cupboard
    - library ferret
    - more axolotl variants api
    - terrablender
    - titanium
    - upgraded core
  - resource packs and data packs
    - better villages more villagers compatibility
    - enemy expansion modded biomes compat
- remove mods
  - storage
    - framed drawers (functionality added by another mod)
    - storage drawers (functionality added by another mod)
  - ui
    - mod name tooltip (functionality added by another mod)
    - shulker box tooltip (functionality added by another mod)

## 1.14

- downgrade forge version to 40.2.9 as the other version had a virus

## 1.13

- remove mods valkyrien skies and eureka air ships
- remove eureka create airships resource pack

## 1.12

- fix xaeros minimap (waypoints) so it does not reset when the server ip changes

## 1.11

- fix xaeros worldmap so it does not reset when the server ip changes
- lower server memory as it may be causing the EC2 instance to freeze up

## 1.10

- update backup script to push the correct zip file to the S3 bucket and then remove the zip file
- completely overhaul aws-user-data and minecraft.service so the server starts up correctly in the EC2 instance
- update README

## 1.9

- fix typo in backup.sh script
- update crontab setup in aws-user-data
- update minecraft.service exec start
- remove whitelist from repo instead it is a GitLab CICD variable that we be injected to the server files when they are generated

## 1.8

- update errors in AWS EC2 server files with cron

## 1.7

- fix bug in infernal mobs that caused a crash if it tried to spawn an inferno mob
- update xaeros world map to disable footsteps by default
- add AWS related server files for running a server in EC2
- add server files
  - this is just the bare minimum files required to be in the repo, the rest of the server files will be generated by a script on release or when the server starts up
- add script to generate server files alogn with a list of required configs and mods for the server
- remove .gitignore
- update .gitlab-ci.yml file to deploy the server files to an S3 bucket and create asset links for the modpack
- remove sword-and-forge datapack and darkmode resource pack archives so they can be generated during release instead
- add backup script and add it as a crontab in EC2 instance

## 1.6

- remove mod journey map
- add mods xaeros minimap, xaeros world map, and xaeros waystones compatibility
- rename reduce-villages data pack to sword-and-forge as it will act as a more general purpose data pack for the modpack
- add function to sword-and-forge data pack that removes terralith welcome text
- update agricraft jei textures in darkmode resource pack
- update create jei textures in darkmode resource pack
- update README mod list
- add agricraft journal model fix to darkmode resource pack
- refresh paxi datapack and resourcepack lists
- update default options

## 1.5

- add mods GUI to darkmode resource pack
  - chicken chunks
  - corpse
  - vs eureka
  - twilight forest
  - tinkers construct
  - trashcans
  - mob grinding utils
  - jei
  - jeresources
  - storage drawers
  - create
  - creative core
  - better advancements
  - guard villagers
  - mowzies mobs
  - polymorph
  - shulker box tooltip
  - stalwart dungeons
  - supermartijn642 core lib
  - waystones
  - inventory profiles next
- remove unused textures from darkmode texture pack
- remove tetra double tool from weapon leveling
- update waystones so generated ones can be broken
- revert towns and towers village spawning back to default values
- add custom data pack, Reduce Villages, that reduces villages for CTOV and terralith mods
- add CTOV and More Villagers compat data pack
- add mods libIPN, inventory profiles next, and inventory tweaks emu for IPN and remove inventory sorter
- add anti enderman grief data pack
- update default settings for inventory profiles next
- update git ignore to ignore zip file for the multimc instance

## 1.4

- add unzipped version of the dark mode resource pack
  - my plan is make this resource pack my own and add all mods in this modpack to it
- add new main menu background
- add curios GUI to darkmode resource pack
- add dungoens gear and libraries GUI to darkmode resource pack
- add rechiseld and aquacraft GUI to darkmode resource pack
- add sophisticated care GUI to darkmode resource pack
- update journey map default them to Stronghold

## 1.3

- decrease infernal mobs chance even more
- fix bug in agricraft with journal model not working
- decrease towns and towers villages frequency slightly (decrease by 1/8)
- increase default music volume
- update curios setting to only show necessary slots

## 1.2

- set complementary shaderpack by default
- turn emmisive lichen off in shaderpack settings
- update journeymap default theme and disable initial splash screen
- remove pmmo
- update sophisticated backpacks so players cannot open one anothers backpacks
- add dungoens gear melee weapons to weapon leveling config and update xp values
- update default betterf3 menu
- create custom darkmode resource pack and add agricraft
- decrease infernal mobs chance

## 1.1

- update general, graphics/performance, and keybinds options
- update jade config
- update ore-excavation whitelisted blocks
- add datapacks and resourcepacks to paxi
- update advancement tracker to be hidden by default
- update cowboyup so it does not report status upon load and horse can swim twice as long (60 sec)
- update dungeons mobs to increase spawn chance of all mobs
- remove tinker construct from list of blacklisted mods in durability tooltip
- disable cascades and splash particles for mod effective
- remove right click to harvest mod because agricraft adds that functionality
- update agricraft to disable weeds, use GUI, and set all plants to revert to stage 1 upon harvest
- increase chance of rotten creatures spawning
- update tinkers construct so player does not spawn with the book and only one variant of tools is shown in JEI
- enable tetra toolbet curios slot compatability
- lower torohealth hide delay
- remove tarvellers titles mod
- update waystones so generated ones cannot be broken
- update journey map default settings and default key from j to m
- add complementary shaders and custom settings for it

## 1.0

- add mod list to the README
- add initial mods
- add multimc-instance directory
- add default generated config files

## 0.1

- Initial Commit
