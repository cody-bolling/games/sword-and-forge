#!/bin/bash

echo "[INFO] Creating Tetra wood materials..."

TETRA_DATA_DIR="resources/datapacks/sword-and-forge/data/tetra"
TETRA_ASSETS_DIR="resources/resourcepacks/sword-and-forge/assets/tetra"
TETRA_SCRIPTS_DIR="scripts/tetra"
CUSTOM_TAGS_DIR="resources/datapacks/sword-and-forge/data/saf/tags/items"
WOOD_FILE="${TETRA_SCRIPTS_DIR}/wood.yaml"
LANG_FILE="${TETRA_ASSETS_DIR}/lang/en_us.json"
TOOLS_FILE="${TETRA_SCRIPTS_DIR}"/tools.yaml
CATEGORY_COUNT=$(yq eval '.woodCategories | length' ${WOOD_FILE})
TOOL_COUNT=$(yq eval '.tools | length' ${TOOLS_FILE})

# Make the wood material directories
mkdir -p ${TETRA_DATA_DIR}/materials/wood
mkdir -p ${TETRA_DATA_DIR}/materials/wood/categories
mkdir -p ${TETRA_DATA_DIR}/materials/wood/compat

# Make custom tetra recipes directory
mkdir -p ${TETRA_DATA_DIR}/recipes

for (( i=0; i < ${TOOL_COUNT}; ++i )); do
  export i
  NAME=$(yq eval '.tools[env(i)].name' ${TOOLS_FILE})

  mkdir -p ${TETRA_DATA_DIR}/recipes/${NAME}
done

# Make custom tags directory
mkdir -p ${CUSTOM_TAGS_DIR}

for (( i=0; i < ${CATEGORY_COUNT}; ++i )); do
  export i

  # Variables from the YAML file
  NAME=$(yq eval '.woodCategories[env(i)].name' ${WOOD_FILE})
  SECONDARY=$(yq eval '.woodCategories[env(i)].stats.secondary' ${WOOD_FILE})
  TERTIARY=$(yq eval '.woodCategories[env(i)].stats.tertiary' ${WOOD_FILE})
  DURABILITY=$(yq eval '.woodCategories[env(i)].stats.durability' ${WOOD_FILE})
  INTEGRITY_GAIN=$(yq eval '.woodCategories[env(i)].stats.integrityGain' ${WOOD_FILE})
  MAGIC_CAPACITY=$(yq eval '.woodCategories[env(i)].stats.magicCapacity' ${WOOD_FILE})
  DEFAULT_COLOR=$(yq eval '.woodCategories[env(i)].stats.defaultColor' ${WOOD_FILE})
  MATERIAL_COUNT=$(yq eval '.woodCategories[env(i)].materials | length' ${WOOD_FILE})

  # Generated variables
  NAME_LOWERCASE=$(echo "$NAME" | tr '[:upper:]' '[:lower:]' | tr ' ' '_')
  CATEGORY_DATA_FILE="${TETRA_DATA_DIR}/materials/wood/categories/${NAME_LOWERCASE}.json"
  MATERIAL_TEMPLATE_FILE="${TETRA_DATA_DIR}/materials/wood/${NAME_LOWERCASE}_template.json"
  CATEGORY_TAG_FILE=${CUSTOM_TAGS_DIR}/${NAME_LOWERCASE}_wood.json

  # Add variables to the category data file
  cp "${TETRA_SCRIPTS_DIR}/wood-template.json" ${CATEGORY_DATA_FILE}
  sed -i "s/\${SECONDARY}/${SECONDARY}/g" ${CATEGORY_DATA_FILE}
  sed -i "s/\${TERTIARY}/${TERTIARY}/g" ${CATEGORY_DATA_FILE}
  sed -i "s/\${DURABILITY}/${DURABILITY}/g" ${CATEGORY_DATA_FILE}
  sed -i "s/\${INTEGRITY_GAIN}/${INTEGRITY_GAIN}/g" ${CATEGORY_DATA_FILE}
  sed -i "s/\${MAGIC_CAPACITY}/${MAGIC_CAPACITY}/g" ${CATEGORY_DATA_FILE}
  cp ${CATEGORY_DATA_FILE} ${MATERIAL_TEMPLATE_FILE}

  # Add more varaibles to the category data file
  sed -i "s/\${NAME}/${NAME_LOWERCASE}_wood/g" ${CATEGORY_DATA_FILE}
  sed -i "s/\${COLOR}/${DEFAULT_COLOR}/g" ${CATEGORY_DATA_FILE}

  # Update lang file with human readable names
  jq --arg key "tetra.material.${NAME_LOWERCASE}_wood" --arg value "${NAME} Wood" '. + {($key): $value}' "${LANG_FILE}" > "${LANG_FILE}.tmp"
  mv "${LANG_FILE}.tmp" "${LANG_FILE}"
  jq --arg key "tetra.material.${NAME_LOWERCASE}_wood.prefix" --arg value "${NAME} Wood" '. + {($key): $value}' "${LANG_FILE}" > "${LANG_FILE}.tmp"
  mv "${LANG_FILE}.tmp" "${LANG_FILE}"

  # Add custom tag to the category data file
  jq --arg TAG "saf:${NAME_LOWERCASE}_wood" '.material.tag = $TAG' "${CATEGORY_DATA_FILE}" > "${CATEGORY_DATA_FILE}.tmp"
  mv "${CATEGORY_DATA_FILE}.tmp" "${CATEGORY_DATA_FILE}"

  # Create custom tag file
  echo "{\"replace\": false, \"values\": []}" > ${CATEGORY_TAG_FILE}

  for (( j=0; j < ${MATERIAL_COUNT}; ++j )); do
    export j

    # Variables from the YAML file
    MATERIAL_ID=$(yq eval '.woodCategories[env(i)].materials[env(j)].id' ${WOOD_FILE})
    MATERIAL_NAME=$(yq eval '.woodCategories[env(i)].materials[env(j)].name' ${WOOD_FILE})
    MATERIAL_COLOR=$(yq eval '.woodCategories[env(i)].materials[env(j)].color' ${WOOD_FILE})

    # Generated variables
    MATERIAL_NAME_LOWERCASE=$(echo "$MATERIAL_NAME" | tr '[:upper:]' '[:lower:]' | tr ' ' '_')
    MATERIAL_DATA_FILE=""

    if [[ $MATERIAL_ID == "minecraft:"* ]]; then
      MATERIAL_DATA_FILE="${TETRA_DATA_DIR}/materials/wood/${MATERIAL_NAME_LOWERCASE}.json"
    else
      MATERIAL_DATA_FILE="${TETRA_DATA_DIR}/materials/wood/compat/${MATERIAL_NAME_LOWERCASE}.json"
    fi

    # Add variables to the material data file
    cp ${MATERIAL_TEMPLATE_FILE} ${MATERIAL_DATA_FILE}
    sed -i "s/\${NAME}/${MATERIAL_NAME_LOWERCASE}/g" ${MATERIAL_DATA_FILE}
    sed -i "s/\${COLOR}/${MATERIAL_COLOR}/g" ${MATERIAL_DATA_FILE}

    # Insert material ID into the material data file
    jq --arg MATERIAL_ID "${MATERIAL_ID}" '.material.items = [$MATERIAL_ID]' "${MATERIAL_DATA_FILE}" > "${MATERIAL_DATA_FILE}.tmp"
    mv "${MATERIAL_DATA_FILE}.tmp" "${MATERIAL_DATA_FILE}"

    # Insert material ID into the category tag file
    jq --arg MATERIAL_ID "${MATERIAL_ID}" '.values += [$MATERIAL_ID]' "${CATEGORY_TAG_FILE}" > "${CATEGORY_TAG_FILE}.tmp"
    mv "${CATEGORY_TAG_FILE}.tmp" "${CATEGORY_TAG_FILE}"

    # Add hidden traits to the material data file
    HIDDEN=true
    jq --arg key "hidden" --argjson value "${HIDDEN}" '. + {($key): $value}' "${MATERIAL_DATA_FILE}" > "${MATERIAL_DATA_FILE}.tmp"
    mv "${MATERIAL_DATA_FILE}.tmp" "${MATERIAL_DATA_FILE}"
    jq --arg key "hiddenOutcomes" --argjson value "${HIDDEN}" '. + {($key): $value}' "${MATERIAL_DATA_FILE}" > "${MATERIAL_DATA_FILE}.tmp"
    mv "${MATERIAL_DATA_FILE}.tmp" "${MATERIAL_DATA_FILE}"

    # Update lang file with human readable name
    jq --arg key "tetra.material.${MATERIAL_NAME_LOWERCASE}" --arg value "${MATERIAL_NAME}" '. + {($key): $value}' "${LANG_FILE}" > "${LANG_FILE}.tmp"
    mv "${LANG_FILE}.tmp" "${LANG_FILE}"
    jq --arg key "tetra.material.${MATERIAL_NAME_LOWERCASE}.prefix" --arg value "${MATERIAL_NAME}" '. + {($key): $value}' "${LANG_FILE}" > "${LANG_FILE}.tmp"
    mv "${LANG_FILE}.tmp" "${LANG_FILE}"

    # Create custom recipe for every material (this will allow for tools to be made in a crafting table)
    for (( k=0; k < ${TOOL_COUNT}; ++k )); do
      export k
      TOOL_NAME=$(yq eval '.tools[env(k)].name' ${TOOLS_FILE})
      RECIPE_COUNT=$(yq eval '.tools[env(k)].recipes | length' ${TOOLS_FILE})

      for (( l=0; l < ${RECIPE_COUNT}; ++l )); do
        export l
        TEMPLATE_RECIPE_FILE_NAME=$(yq eval '.tools[env(k)].recipes[env(l)]' ${TOOLS_FILE})
        RECIPE_FILE=${TETRA_DATA_DIR}/recipes/${TOOL_NAME}/${MATERIAL_NAME_LOWERCASE}.json

        cp ${TETRA_SCRIPTS_DIR}/recipe-templates/${TEMPLATE_RECIPE_FILE_NAME} ${RECIPE_FILE}
        sed -i "s/\${MATERIAL}/${MATERIAL_NAME_LOWERCASE}/g" ${RECIPE_FILE}
        sed -i "s/\${MATERIAL_ID}/${MATERIAL_ID}/g" ${RECIPE_FILE}
      done
    done
  done

  rm -fr ${MATERIAL_TEMPLATE_FILE}
done
