#!/bin/bash

DATAPACKS_DIR=resources/datapacks
RESOURCEPACKS_DIR=resources/resourcepacks
SHADERPACKS_DIR=resources/shaderpacks
DIRECTORIES=("${DATAPACKS_DIR}" "${RESOURCEPACKS_DIR}" "${SHADERPACKS_DIR}")

echo "[INFO] Creating resource archives..."

for DIR in "${DIRECTORIES[@]}"; do
  SUBDIRECTORIES=("${DIR}"/*/)

  echo "[INFO] Processing ${DIR}..."
  pushd ${DIR}
  
  for SUBDIR in "${SUBDIRECTORIES[@]}"; do
    SUBDIR_NAME=$(basename "${SUBDIR}")
    
    echo "[INFO] Archiving $SUBDIR_NAME..."

    pushd ${SUBDIR_NAME}
    zip -r "${SUBDIR_NAME}.zip" ./*
    popd

    rm -fr "./${SUBDIR_NAME}.zip"
    mv "${SUBDIR_NAME}/${SUBDIR_NAME}.zip" "./${SUBDIR_NAME}.zip"
  done

  popd

  DESTINATION_DIR=""

  if [[ "${DIR}" == "${DATAPACKS_DIR}" ]]; then
    DESTINATION_DIR="sword-and-forge/.minecraft/config/paxi/datapacks"
  elif [[ "${DIR}" == "${RESOURCEPACKS_DIR}" ]]; then
    DESTINATION_DIR="sword-and-forge/.minecraft/config/paxi/resourcepacks"
  elif [[ "${DIR}" == "${SHADERPACKS_DIR}" ]]; then
    DESTINATION_DIR="sword-and-forge/.minecraft/shaderpacks"
  fi

  echo "[INFO] Copying all files from ${DIR} to ${DESTINATION_DIR}"
  mkdir -p ${DESTINATION_DIR}

  for FILE in "${DIR}"/*; do
    if [ -f "$FILE" ]; then
      cp -n "$FILE" "$DESTINATION_DIR"
    fi
  done
done

echo "[INFO] Creating modpack archive..."
zip -r sword-and-forge.zip sword-and-forge/
