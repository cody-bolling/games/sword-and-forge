#!/bin/sh
#
# NOTE: Requires yq to run.
#
# Copies the files listed in server/required-files.yaml to server/server/ and generates a server.zip file.
#

REQ_FILES_PATH="server/required-files.yaml"
REQ_CONFIGS_COUNT=$(yq eval '.required-configs | length' ${REQ_FILES_PATH})
REQ_MODS_COUNT=$(yq eval '.required-mods | length' ${REQ_FILES_PATH})

echo "[INFO] Creating whitelist..."
echo ${WHITELIST} > server/server/whitelist.json

echo "[INFO] Copying required config files..."

for (( i=0; i < ${REQ_CONFIGS_COUNT}; ++i )); do
  export i
  FILE_PATH=$(yq eval '.required-configs[env(i)]' ${REQ_FILES_PATH})
  IFS='/'; read -ra FILE_PATH_ARRAY <<< "${FILE_PATH}"; IFS=
  unset 'FILE_PATH_ARRAY[${#aFILE_PATH_ARRAYrr[@]}-1]'
  _FILE_PATH=""

  for DIRECTORY in "${FILE_PATH_ARRAY[@]}"; do
    _FILE_PATH="${_FILE_PATH}${DIRECTORY}/"
  done

  echo "[INFO] Copying ${_FILE_PATH}"
  mkdir -p server/server/${_FILE_PATH}
  cp sword-and-forge/.minecraft/${FILE_PATH} server/server/${_FILE_PATH}
done

echo "[INFO] Copying crafttweaker scripts..."
cp -r sword-and-forge/.minecraft/scripts server/server/scripts

echo "[INFO] Copying patchouli books..."
cp -r sword-and-forge/.minecraft/patchouli_books server/server/patchouli_books

echo "[INFO] Copying required mod files..."

mkdir -p server/server/mods

for (( i=0; i < ${REQ_MODS_COUNT}; ++i )); do
  FILE_PATH=$(yq eval '.required-mods[env(i)]' ${REQ_FILES_PATH})

  echo "[INFO] Copying ${FILE_PATH}"
  cp sword-and-forge/.minecraft/mods/${FILE_PATH} server/server/mods/${FILE_PATH}
done

echo "[INFO] Creating server archive..."
pushd server
zip -r server.zip ./server/*
popd

mv server/server.zip server.zip
