#!/bin/bash

echo "[INFO] Uploading modpack and server archives as release assets..."
curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file sword-and-forge.zip ${PACKAGE_REGISTRY_URL}/sword-and-forge.zip
curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file server.zip ${PACKAGE_REGISTRY_URL}/sword-and-forge-server.zip
