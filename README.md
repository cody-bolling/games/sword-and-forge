![Project Avatar](repo-icon.png)

# Sword and Forge

**A minecraft modpack that primarily focuses on improving every aspect of vanilla minecraft.**

- Adds new world generation, structures, villages, villagers, and dimensions to help improve the exploration aspect of minecraft.

- Adds new weapons, armor, mobs, and combat animations to make fighting your way through the world a blast.

- Improves upon fishing and farming to give those features a bit of extra depth making them more engaging and enjoyable.

- Adds a few tech mods including Create, Tetra, and Tinkers Construct.

- Adds the chisel and other decoration block mods to help give life to builds.

- Tons of quality of life, performance improvemnts, new music, and custom dark UI for every mod to make the minecraft experience better than ever.

- And so much more! See full mod list below.

<details>
<summary>Project Structure</summary>

```text
sword-and-forge
│ README.md
│ CHANGELOG.md
| .gitlab-ci.yml
| repo-icon.png
│
└─ data-packs
    │ Contains the data pack files
└─ resource-packs
    │ Contains the resource pack files
└─ server
    │ Contains all server related files
└─ Sword and Forge
    │ Contains all multimc modpack files including default configs and mods
```

</details>
<br>

# Mod List

List of every mod in this modpack catgeorized and with a link to each Curseforge page.

## Big Tech

- [Create](https://www.curseforge.com/minecraft/mc-mods/create)
- [Create: Alloyed](https://www.curseforge.com/minecraft/mc-mods/create-alloyed)
- [Create: Central Kitchen](https://modrinth.com/mod/create-central-kitchen)
- [Create: Chunkloading](https://www.curseforge.com/minecraft/mc-mods/create-chunkloading)
- [Create: Crystal Clear](https://modrinth.com/mod/create-crystal-clear)
- [Create: Diesel Generators](https://modrinth.com/mod/create-diesel-generators)
- [Create: Encased](https://www.curseforge.com/minecraft/mc-mods/create-encased)
- [Create: Enchantment Industry](https://www.curseforge.com/minecraft/mc-mods/create-enchantment-industry)
- [Create: Extended Cogwheels](https://www.curseforge.com/minecraft/mc-mods/create-extended-cogs)
- [Create: Jetpack](https://www.curseforge.com/minecraft/mc-mods/create-jetpack)
- [Create: Liquid Burner](https://www.curseforge.com/minecraft/mc-mods/liquid-burner)
- [Create: Mechanical Spawner](https://modrinth.com/mod/create-mechanical-spawner)
- [Create: New Age](https://modrinth.com/mod/create-new-age)
- [Create: Slice and Dice](https://modrinth.com/mod/slice-and-dice)
- [Create: Steam 'n' Rails](https://www.curseforge.com/minecraft/mc-mods/create-steam-n-rails)
- [Create: Structures](https://modrinth.com/datapack/create-structures)
- [Create: Tipsy n' Dizzy](https://modrinth.com/mod/create-tipsy-n-high)

## Bug Fixes

- [Async Locator](https://modrinth.com/mod/async-locator)
- [Attribute Fix](https://www.curseforge.com/minecraft/mc-mods/attributefix)
- [Bad Horse Fix](https://modrinth.com/mod/bad-horse-fix)
- [Fix GPU Memory Leak](https://www.curseforge.com/minecraft/mc-mods/fix-gpu-memory-leak)
- [Model Gap Fix](https://www.curseforge.com/minecraft/mc-mods/model-gap-fix)
- [Nether Portal Fix](https://www.curseforge.com/minecraft/mc-mods/netherportalfix)
- [No Chat Reports](https://www.curseforge.com/minecraft/mc-mods/no-chat-reports)
- [No Telemetry](https://modrinth.com/mod/no-telemetry/versions)

## Content Addons

- [Born in Chaos](https://www.curseforge.com/minecraft/mc-mods/born-in-chaos)
- [Environmental](https://www.curseforge.com/minecraft/mc-mods/environmental)
- [Savage and Ravage](https://www.curseforge.com/minecraft/mc-mods/savage-and-ravage)
- [Upgrade Aquatic](https://www.curseforge.com/minecraft/mc-mods/upgrade-aquatic)

## Decoration

- [Decorative Blocks](https://www.curseforge.com/minecraft/mc-mods/decorative-blocks)
- [Exposure](https://www.curseforge.com/minecraft/mc-mods/exposure)
- [Fusion](https://www.curseforge.com/minecraft/mc-mods/fusion-connected-textures)
- [MmmMmmMmmMmm (Target Dummy)](https://www.curseforge.com/minecraft/mc-mods/mmmmmmmmmmmm)
- [Rechiseled](https://www.curseforge.com/minecraft/mc-mods/rechiseled)
- [Rechiseled: Create](https://www.curseforge.com/minecraft/mc-mods/rechiseled-create)
- [Supplementaries](https://www.curseforge.com/minecraft/mc-mods/supplementaries)
- [Tetra Tables Looking Neat](https://modrinth.com/mod/tetra-tables-looking-neat)
- [Workshop for Handsome Adventurer](https://modrinth.com/mod/workshop-for-handsome-adventurer)

## Dimensions

- [Aether](https://www.curseforge.com/minecraft/mc-mods/aether)
  - [Custom Patch](https://github.com/Cody-Bolling/The-Aether)
- [Aether Redux](https://www.curseforge.com/minecraft/mc-mods/aether-redux)
- [Deep Aether](https://www.curseforge.com/minecraft/mc-mods/deep-aether)
- [Deeper Darker](https://www.curseforge.com/minecraft/mc-mods/deeperdarker)
- [Twilight Forest](https://www.curseforge.com/minecraft/mc-mods/the-twilight-forest)
- [Undergarden](https://www.curseforge.com/minecraft/mc-mods/the-undergarden)

## Dungeons

- [Bygone Nether](https://www.curseforge.com/minecraft/mc-mods/bygone-nether)
- [Dungeon Crawl](https://www.curseforge.com/minecraft/mc-mods/dungeon-crawl)
- [L_Ender's Cataclysm](https://www.curseforge.com/minecraft/mc-mods/l_ender-s-cataclysm)
- [Stalwart Dungeons](https://www.curseforge.com/minecraft/mc-mods/stalwart-dungeons)

## Farming and Fishing

- [Aether Delight](https://www.curseforge.com/minecraft/mc-mods/aether-delight)
- [Alex's Delight](https://www.curseforge.com/minecraft/mc-mods/alexs-delight)
- [Aquaculture](https://www.curseforge.com/minecraft/mc-mods/aquaculture)
- [Aquaculture Delight](https://www.curseforge.com/minecraft/mc-mods/aquaculture-delight)
- [Farmer's Delight](https://www.curseforge.com/minecraft/mc-mods/farmers-delight)
- [Nether's Delight](https://www.curseforge.com/minecraft/mc-mods/nethers-delight)
- [Undergarden Delight](https://www.curseforge.com/minecraft/mc-mods/undergarden-delight)

## Mobs

- [Alex's Mobs](https://www.curseforge.com/minecraft/mc-mods/alexs-mobs)
- [Creatures and Beasts](https://www.curseforge.com/minecraft/mc-mods/creatures-and-beasts)
- [Creeper Overhaul](https://www.curseforge.com/minecraft/mc-mods/creeper-overhaul)
- [Dungeons Mobs](https://www.curseforge.com/minecraft/mc-mods/dungeons-mobs)
- [Ender Dragon Fight Remastered](https://modrinth.com/datapack/edf-remastered)
- [Infernal Mobs](https://www.curseforge.com/minecraft/mc-mods/atomicstrykers-infernal-mobs)
- [More Axolotl Variants](https://www.curseforge.com/minecraft/mc-mods/mavm)
- [More Mob Variants](https://www.curseforge.com/minecraft/mc-mods/more-mob-variants)
- [Mowzie's Mobs](https://www.curseforge.com/minecraft/mc-mods/mowzies-mobs)
- [Nether Skeletons](https://www.curseforge.com/minecraft/mc-mods/nether-skeletons)
- [Rotten Creatures](https://www.curseforge.com/minecraft/mc-mods/rotten-creatures)

## Performance

- [Canary](https://www.curseforge.com/minecraft/mc-mods/canary)
- [Chunk Sending](https://www.curseforge.com/minecraft/mc-mods/chunk-sending-forge-fabric)
- [Cull Less Leaves Reforged](https://www.curseforge.com/minecraft/mc-mods/culllessleaves-reforged)
- [Dynamic FPS](https://modrinth.com/mod/dynamic-fps)
- [Embeddium](https://www.curseforge.com/minecraft/mc-mods/embeddium)
- [Embeddium++](https://modrinth.com/mod/embeddiumplus)
- [Entity Collision FPS Fix](https://www.curseforge.com/minecraft/mc-mods/entity-collision-fps-fix)
- [Entity Culling](https://www.curseforge.com/minecraft/mc-mods/entityculling)
- [Fast Paintings](https://www.curseforge.com/minecraft/mc-mods/fast-paintings)
- [Fast Workbench](https://www.curseforge.com/minecraft/mc-mods/fastworkbench)
- [Ferrite Core](https://www.curseforge.com/minecraft/mc-mods/ferritecore)
- [Lazy DFU](https://www.curseforge.com/minecraft/mc-mods/lazy-dfu-forge)
- [Let Me Despawn](https://www.curseforge.com/minecraft/mc-mods/let-me-despawn)
- [Lightspeed](https://www.curseforge.com/minecraft/mc-mods/lightspeedmod)
- [Oculus](https://www.curseforge.com/minecraft/mc-mods/oculus)
- [Oculus Flywheel Compat](https://www.curseforge.com/minecraft/mc-mods/iris-flywheel-compat)
- [Oculus GeckoLib Compat](https://modrinth.com/mod/geckoanimfix/versions)
- [Oculus Particle Fix](https://modrinth.com/mod/oculus-particle-fix/versions)
- [Pluto](https://www.curseforge.com/minecraft/mc-mods/pluto)
- [Redirector](https://www.curseforge.com/minecraft/mc-mods/redirectionor)
- [Saturn](https://www.curseforge.com/minecraft/mc-mods/saturn)
- [Smooth Boot (Reloaded)](https://www.curseforge.com/minecraft/mc-mods/smooth-boot-reloaded)
- [Smooth Chunk Save](https://www.curseforge.com/minecraft/mc-mods/smooth-chunk-save)
- [Starlight](https://www.curseforge.com/minecraft/mc-mods/starlight-forge)

## QoL

- [Boatload](https://www.curseforge.com/minecraft/mc-mods/boatload)
- [Bridging Mod](https://modrinth.com/mod/bridging-mod)
- [Carry On](https://www.curseforge.com/minecraft/mc-mods/carry-on)
- [Chicken Chunks](https://www.curseforge.com/minecraft/mc-mods/chicken-chunks-1-8)
- [Client Tweaks](https://www.curseforge.com/minecraft/mc-mods/client-tweaks)
- [Clumps](https://www.curseforge.com/minecraft/mc-mods/clumps)
- [Controlling](https://www.curseforge.com/minecraft/mc-mods/controlling)
- [Corpse](https://www.curseforge.com/minecraft/mc-mods/corpse)
- [Crafting Tweaks](https://modrinth.com/mod/crafting-tweaks)
- [Easier Sleeping](https://www.curseforge.com/minecraft/mc-mods/easier-sleeping)
- [Easy Anvils](https://www.curseforge.com/minecraft/mc-mods/easy-anvils)
- [Easy Magic](https://www.curseforge.com/minecraft/mc-mods/easy-magic)
- [Easy Shulker Boxes](https://www.curseforge.com/minecraft/mc-mods/easy-shulker-boxes)
- [Enchantment Descriptions](https://modrinth.com/mod/enchantment-descriptions)
- [Inventory Profiles Next](https://www.curseforge.com/minecraft/mc-mods/inventory-profiles-next)
- [Inventory Tweaks Emu for IPN](https://modrinth.com/mod/invtweaks-emu-for-ipn/version/forge-1.18.2-1.0.7)
- [JEI](https://www.curseforge.com/minecraft/mc-mods/jei)
- [JER Integration](https://www.curseforge.com/minecraft/mc-mods/jer-integration)
- [Journey Map](https://modrinth.com/mod/journeymap)
- [Journey Map Integration](https://modrinth.com/mod/journeymap-integration)
- [Just Enough Resources](https://www.curseforge.com/minecraft/mc-mods/just-enough-resources-jer)
- [Just Zoom](https://modrinth.com/mod/just-zoom)
- [Klee Slabs](https://www.curseforge.com/minecraft/mc-mods/kleeslabs)
- [Leaves be Gone](https://www.curseforge.com/minecraft/mc-mods/leaves-be-gone)
- [Login Protection](https://www.curseforge.com/minecraft/mc-mods/login-protection)
- [More Overlays](https://www.curseforge.com/minecraft/mc-mods/more-overlays-updated)
- [Mouse Tweaks](https://www.curseforge.com/minecraft/mc-mods/mouse-tweaks)
- [Nature's Compass](https://www.curseforge.com/minecraft/mc-mods/natures-compass)
- [Ore Excavation](https://www.curseforge.com/minecraft/mc-mods/ore-excavation)
- [Player Revive](https://www.curseforge.com/minecraft/mc-mods/playerrevive)
- [Right Click Harvest](https://www.curseforge.com/minecraft/mc-mods/rightclickharvest)
- [Structure Compass](https://modrinth.com/mod/structure-compass)
- [Trade Cycling](https://www.curseforge.com/minecraft/mc-mods/trade-cycling)
- [Waystones](https://www.curseforge.com/minecraft/mc-mods/waystones)

## Storage

- [Astikor Carts](https://www.curseforge.com/minecraft/mc-mods/astikorcarts)
- [Ender Storage](https://www.curseforge.com/minecraft/mc-mods/ender-storage-1-8)
- [Functional Storage](https://www.curseforge.com/minecraft/mc-mods/functional-storage)

## UI

- [Apple Skin](https://www.curseforge.com/minecraft/mc-mods/appleskin)
- [Better Advancements](https://www.curseforge.com/minecraft/mc-mods/better-advancements)
- [BetterF3](https://www.curseforge.com/minecraft/mc-mods/betterf3)
- [Better Statistics Screen](https://www.curseforge.com/minecraft/mc-mods/better-stats)
- [Drippy Loading Screen](https://www.curseforge.com/minecraft/mc-mods/drippy-loading-screen)
- [Durability Tooltip](https://www.curseforge.com/minecraft/mc-mods/durability-tooltip)
- [Fancy Menu](https://www.curseforge.com/minecraft/mc-mods/fancymenu-forge)
- [Jade](https://www.curseforge.com/minecraft/mc-mods/jade)
- [Jade Addons](https://www.curseforge.com/minecraft/mc-mods/jade-addons)
- [Legendary Tooltips](https://www.curseforge.com/minecraft/mc-mods/legendary-tooltips)
  - [Custom Patch](https://github.com/Cody-Bolling/LegendaryTooltips)
- [Overflowing Bars](https://www.curseforge.com/minecraft/mc-mods/overflowing-bars)
- [Showcase Item](https://www.curseforge.com/minecraft/mc-mods/showcase-item)
- [Stylish Effects](https://www.curseforge.com/minecraft/mc-mods/stylish-effects)
- [Toast Control](https://www.curseforge.com/minecraft/mc-mods/toast-control)
- [ToroHealth Damage Indicators](https://www.curseforge.com/minecraft/mc-mods/torohealth-damage-indicators)

## Villagers

- [Guard Villagers](https://www.curseforge.com/minecraft/mc-mods/guard-villagers)

## Visual

- [Better Foliage Renewed](https://www.curseforge.com/minecraft/mc-mods/better-foliage-renewed)
- [Curious Lanterns](https://www.curseforge.com/minecraft/mc-mods/curious-lanterns)
  - [Custom Patch](https://github.com/Cody-Bolling/CuriousLanterns)
- [Effective](https://www.curseforge.com/minecraft/mc-mods/effective)
- [Embeddium Dyanmic Lights](https://www.curseforge.com/minecraft/mc-mods/dynamiclights-reforged)
- [Excavated Variants](https://modrinth.com/mod/excavated_variants)
- [Item Physics Full](https://www.curseforge.com/minecraft/mc-mods/itemphysic)
- [Not Enough Animations](https://www.curseforge.com/minecraft/mc-mods/not-enough-animations)
- [Public GUI Announcement](https://www.curseforge.com/minecraft/mc-mods/public-gui-announcement)
- [Radiant Gear](https://modrinth.com/mod/radiant-gear)
- [Skin Layers 3D](https://www.curseforge.com/minecraft/mc-mods/skin-layers-3d)

## Weapons, Armor, and Combat

- [Better Combat](https://www.curseforge.com/minecraft/mc-mods/better-combat-by-daedelus)
- [Curios API](https://www.curseforge.com/minecraft/mc-mods/curios)
- [Curios Elytra Slot](https://www.curseforge.com/minecraft/mc-mods/elytra-slot)
- [Dungeons Gear](https://www.curseforge.com/minecraft/mc-mods/dungeons-gear)
- [Shield Expansion](https://www.curseforge.com/minecraft/mc-mods/shield-expansion)
- [Sophisticated Backpacks](https://www.curseforge.com/minecraft/mc-mods/sophisticated-backpacks)
- [Tetra](https://www.curseforge.com/minecraft/mc-mods/tetra)
- [Tetratic Combat](https://www.curseforge.com/minecraft/mc-mods/tetratic-combat)
- [Upgraded Netherite](https://www.curseforge.com/minecraft/mc-mods/upgraded-netherite)

## World Generation and Structures

- [Amplified Nether](https://www.curseforge.com/minecraft/mc-mods/amplified-nether)
- [Better Villages](https://www.curseforge.com/minecraft/mc-mods/better-village-forge)
- [Endless Biomes](https://www.curseforge.com/minecraft/mc-mods/endless-biomes)
- [End's Phantasm](https://www.curseforge.com/minecraft/mc-mods/phantasm)
- [Explorify](https://www.curseforge.com/minecraft/mc-mods/explorify)
- [Sparse Structures Reforged](https://www.curseforge.com/minecraft/mc-mods/ssr)
- [Structory](https://www.curseforge.com/minecraft/mc-mods/structory)
- [Terralith](https://www.curseforge.com/minecraft/mc-mods/terralith)
- [Towns and Towers](https://www.curseforge.com/minecraft/mc-mods/towns-and-towers)
- [Unusual End](https://www.curseforge.com/minecraft/mc-mods/unusual-end)
- [YUNG's Better Desert Temples](https://www.curseforge.com/minecraft/mc-mods/yungs-better-desert-temples)
- [YUNG's Better Dungeons](https://www.curseforge.com/minecraft/mc-mods/yungs-better-dungeons)
- [YUNG's Better End Island](https://www.curseforge.com/minecraft/mc-mods/yungs-better-end-island)
- [YUNG's Better Jungle Temple](https://www.curseforge.com/minecraft/mc-mods/yungs-better-jungle-temples)
- [YUNG's Better Mineshafts](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-forge)
- [YUNG's Better Nether Fortress](https://www.curseforge.com/minecraft/mc-mods/yungs-better-nether-fortresses)
- [YUNG's Better Ocean Monuments](https://www.curseforge.com/minecraft/mc-mods/yungs-better-ocean-monuments)
- [YUNG's Better Strongholds](https://www.curseforge.com/minecraft/mc-mods/yungs-better-strongholds)

## Misc

- [Chunky](https://modrinth.com/plugin/chunky)
- [Craft Tweaker](https://www.curseforge.com/minecraft/mc-mods/crafttweaker)
- [Every Compat (Wood Good)](https://www.curseforge.com/minecraft/mc-mods/every-compat)
- [Global GameRules](https://modrinth.com/mod/global-gamerules)
- [In Control!](https://modrinth.com/mod/in-control)
- [JEI Tweaker](https://www.curseforge.com/minecraft/mc-mods/jeitweaker)
- [Loot Integrations](https://www.curseforge.com/minecraft/mc-mods/loot-integrations)
- [My Server is Compatible](https://modrinth.com/mod/my-server-is-compatible)
- [Patchouli](https://www.curseforge.com/minecraft/mc-mods/patchouli)
- [Paxi](https://www.curseforge.com/minecraft/mc-mods/paxi)
- [Polymorph](https://www.curseforge.com/minecraft/mc-mods/polymorph)
- [Simple Voice Chat](https://www.curseforge.com/minecraft/mc-mods/simple-voice-chat)
- [Sword and Forge Patch](https://gitlab.com/cody-bolling/games/mods/sword-and-forge-patch)
- [Yeetus Experimentus](https://www.curseforge.com/minecraft/mc-mods/yeetusexperimentus)

## Library

- [Aeroblender](https://modrinth.com/mod/aeroblender)
- [Archaeology API](https://www.curseforge.com/minecraft/mc-mods/archaeology-api)
- [Architectury API](https://www.curseforge.com/minecraft/mc-mods/architectury-api)
- [Balm](https://www.curseforge.com/minecraft/mc-mods/balm)
- [Blueprint](https://www.curseforge.com/minecraft/mc-mods/blueprint)
- [Bookshelf](https://www.curseforge.com/minecraft/mc-mods/bookshelf)
- [Botarium](https://modrinth.com/mod/botarium)
- [Caelus API](https://www.curseforge.com/minecraft/mc-mods/caelus)
- [Citadel](https://www.curseforge.com/minecraft/mc-mods/citadel)
- [Cloth Config](https://www.curseforge.com/minecraft/mc-mods/cloth-config)
- [Code Chicken Lib](https://www.curseforge.com/minecraft/mc-mods/codechicken-lib-1-8)
- [Creative Core](https://www.curseforge.com/minecraft/mc-mods/creativecore)
- [Cupboard](https://www.curseforge.com/minecraft/mc-mods/cupboard)
- [Dungeons Libraries](https://www.curseforge.com/minecraft/mc-mods/dungeons-libraries)
- [Dynamic Asset Generator](https://modrinth.com/mod/dynamic_asset_generator)
- [Gecko Lib](https://www.curseforge.com/minecraft/mc-mods/geckolib)
  - [Custom patch](https://github.com/Cody-Bolling/geckolib)
- [Iceberg](https://www.curseforge.com/minecraft/mc-mods/iceberg)
- [Koncrete](https://www.curseforge.com/minecraft/mc-mods/konkrete)
- [Kotlin for Forge](https://www.curseforge.com/minecraft/mc-mods/kotlin-for-forge)
- [Lib IPN](https://www.curseforge.com/minecraft/mc-mods/libipn)
- [Library Ferret](https://www.curseforge.com/minecraft/mc-mods/library-ferret-forge)
- [Lionfish API](https://www.curseforge.com/minecraft/mc-mods/lionfish-api)
- [Melody](https://www.curseforge.com/minecraft/mc-mods/melody)
- [More Axolotl Variants API](https://www.curseforge.com/minecraft/mc-mods/mavapi)
- [Moonlight Lib](https://www.curseforge.com/minecraft/mc-mods/selene)
- [Mutil](https://www.curseforge.com/minecraft/mc-mods/mutil)
- [Placebo](https://www.curseforge.com/minecraft/mc-mods/placebo)
- [Player Animator](https://www.curseforge.com/minecraft/mc-mods/playeranimator)
- [Prism](https://www.curseforge.com/minecraft/mc-mods/prism-lib)
- [Puzzles Lib](https://www.curseforge.com/minecraft/mc-mods/puzzles-lib)
- [Reforgium](https://www.curseforge.com/minecraft/mc-mods/reforgium)
- [Resourceful Config](https://www.curseforge.com/minecraft/mc-mods/resourceful-config)
- [Sophisticated Core](https://www.curseforge.com/minecraft/mc-mods/sophisticated-core)
- [SuperMartijn642's Config Lib](https://www.curseforge.com/minecraft/mc-mods/supermartijn642s-config-lib)
- [SuperMartijn642's Core Lib](https://www.curseforge.com/minecraft/mc-mods/supermartijn642s-core-lib)
- [Terrablender](https://modrinth.com/mod/terrablender)
- [Titanium](https://www.curseforge.com/minecraft/mc-mods/titanium)
- [Upgraded Core](https://www.curseforge.com/minecraft/mc-mods/upgraded-core)
- [YUNG's API](https://www.curseforge.com/minecraft/mc-mods/yungs-api)

# Data Packs, Resource Packs, and Shader Packs

- [Complimentary Shaders](https://modrinth.com/shader/complementary-unbound)
- Custom Sword and Forge data pack made by yours truly
- [Darkmode](https://www.curseforge.com/minecraft/texture-packs/darkmode)
  - I am only using the vanilla GUI textures from the above pack in my sword-and-forge-dark-ui resource pack and have made the modded GUIs myself.
- [Epoch](https://modrinth.com/shader/epoch)
- [Vanilla Tweaks](https://vanillatweaks.net/)
- [VECTOR](https://modrinth.com/shader/vector)
